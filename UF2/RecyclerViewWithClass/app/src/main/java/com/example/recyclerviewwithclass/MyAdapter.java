package com.example.recyclerviewwithclass;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    ArrayList<Animals> animals;
    Context context;

    public MyAdapter(ArrayList<Animals> animals, Context context) {
        this.animals = animals;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.animal_row, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(animals.get(position).getUrlImage()).fit().centerCrop().into(holder.imageView);
        holder.textType.setText(animals.get(position).getType());
        holder.nickname.setText(animals.get(position).getNickname());
        holder.age.setText(String.valueOf(animals.get(position).getAge()));
        holder.birthYear.setText(String.valueOf(animals.get(position).getBirthYear()));
        holder.textMeals.setText(animals.get(position).getMealsAnimal());

        holder.rowLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("type", animals.get(position).getType());
                intent.putExtra("nickname", animals.get(position).getNickname());
                intent.putExtra("age", animals.get(position).getAge());
                intent.putExtra("birthyear", animals.get(position).getBirthYear());
                intent.putExtra("meals", animals.get(position).getMealsAnimal());
                intent.putExtra("desc", animals.get(position).getDesc());

                intent.putExtra("urlImage", animals.get(position).getUrlImage());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return animals.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private TextView textType;
        private TextView nickname;
        private TextView age;
        private TextView birthYear;
        private TextView textMeals;

        ConstraintLayout rowLayout;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView);
            textType = itemView.findViewById(R.id.type);
            nickname = itemView.findViewById(R.id.textNickname);
            age = itemView.findViewById(R.id.textAge);
            birthYear = itemView.findViewById(R.id.textBirthday);
            textMeals = itemView.findViewById(R.id.textMeals);

            rowLayout = itemView.findViewById(R.id.myRow);
        }
    }
}
