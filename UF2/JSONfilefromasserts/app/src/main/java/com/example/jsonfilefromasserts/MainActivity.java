package com.example.jsonfilefromasserts;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    List<String> peakNames = new ArrayList<>();
    List<String> peakHeight = new ArrayList<>();
    List<String> peakImages = new ArrayList<>();
    List<String> peakCountry = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hook
        recyclerView = findViewById(R.id.recyclerView);

        //Layout
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        //Instanciar MyAdapter i assignar-lo al RecyclerView
        MyAdapter myAdapter = new MyAdapter(MainActivity.this, peakNames, peakHeight, peakImages, peakCountry);
        recyclerView.setAdapter(myAdapter);

        //En caso de que el fichero json empieza por un "{"
        try {
            //Getting JSONObject from file
            JSONObject obj = new JSONObject(loadJSONObjectFromAsserts());

            //obtener el JSONArray aqui
            JSONArray peakArray = obj.getJSONArray("peaks");

            for (int i = 0; i < peakArray.length(); i++) {
                JSONObject peakDetail = peakArray.getJSONObject(i);

                //afegir les dades a cada un dels ArrayList
                peakNames.add(peakDetail.getString("name"));
                peakHeight.add(peakDetail.getString("height"));
                peakCountry.add(peakDetail.getString("country"));
                peakImages.add(peakDetail.getString("url"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*
        //En caso de que el fichero json empieza por un "["
        //En aquest cas hem de instanciar primer el JSONArray amb el contingut del fitxer convertit en String.
        //Un cop ja tenim el array de dades, podem iterar i llegir els camps.

        //1. Getting JSON
        try {
            //2. Fetch JSONArray using String
            JSONArray peakArray = new JSONArray(loadJSONObjectFromAsserts());

            //3. Implement a loop because there are many objects inside the array
            for (int i = 0; i < peakArray.length(); i++){
                //4.Creating a JSON object
                JSONObject peakDetail = peakArray.getJSONObject(i);
                //5. Fetching name and height and url
                peakNames.add(peakDetail.getString("name"));
                peakHeight.add(peakDetail.getString("height"));
                peakCountry.add(peakDetail.getString("url"));
                peakImages.add(peakDetail.getString("country"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
    }


    private String loadJSONObjectFromAsserts() {
        String json = null;
        try {
            InputStream input = getAssets().open("peaks.json");

            //Returns an estimate of the number of bytes that can be read
            int size = input.available();

            //Reads some number of bytes from the InputStream and stores them into buffer array.
            byte[] buffer = new byte[size];
            input.read(buffer);
            input.close();
            json = new String(buffer, "UTF-8"); //accpet all char types

        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }


}