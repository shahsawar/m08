package com.example.recyclerviewgridlayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity {

    private TextView text;
    private TextView desc;

    String getText;
    String getDesc;
    ArrayList<String> getImages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //Hook
        text = findViewById(R.id.textDetail);
        desc = findViewById(R.id.descDetail);

        getData();
        setData();

        /*Como el Custom ViewPager hereda del ViewPager pues en este caso voy a usar CustomViewPager.
         *Pero puedo usar ViewPager y funciona.*/
        //ViewPager viewPager = findViewById(R.id.viewPager);
        CustomViewPager customViewPager = findViewById(R.id.viewPager);

        ImageAdapter imageAdapter = new ImageAdapter(this, getImages);
        customViewPager.setAdapter(imageAdapter);

    }

    private void getData() {
        if (getIntent().hasExtra("text") && getIntent().hasExtra("desc")) {
            getText = getIntent().getStringExtra("text");
            getDesc = getIntent().getStringExtra("desc");
            getImages = getIntent().getStringArrayListExtra("images");
        } else {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData() {
        text.setText(getText);
        desc.setText(getDesc);
    }
}