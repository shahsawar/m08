package com.example.recyclerviewgridlayout;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ImageAdapter extends PagerAdapter {
    private Context context;
    private ArrayList<String> mImagesIds;

    public ImageAdapter(Context context, ArrayList<String> mImagesIds) {
        this.context = context;
        this.mImagesIds = mImagesIds;
    }

    @Override
    public int getCount() {
        return mImagesIds.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        //return false;
        return view.equals(object);
        //return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //super.destroyItem(container, position, object);
        container.removeView((View) object);
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        //return super.instantiateItem(container, position);
        PhotoView photoView = new PhotoView(context);
        photoView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        Picasso.get().load(mImagesIds.get(position))
                .fit()
                .centerCrop()
                .into(photoView);

        container.addView(photoView, 0);

        return photoView;

        /*
        ImageView imageView = new ImageView(context);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Picasso.get().load(mImagesIds.get(position)).into(imageView);
        container.addView(imageView, 0);
        return imageView;*/
    }
}
