package com.example.recyclerviewgridlayout;

import java.util.ArrayList;

public class Picture {

    private String text;
    private String url;
    private String desc;
    private ArrayList<String> urlImages;

    public Picture(String text, String url, String desc, ArrayList<String> urlImages) {
        this.text = text;
        this.url = url;
        this.desc = desc;
        this.urlImages = urlImages;
    }

    public ArrayList<String> getUrlImages() {
        return urlImages;
    }

    public void setUrlImages(ArrayList<String> urlImages) {
        this.urlImages = urlImages;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
