package com.example.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    String title[];
    String desc[];
    int images[] = {
        R.drawable.bike,
        R.drawable.boat,
        R.drawable.car,
        R.drawable.construction,
        R.drawable.container,
        R.drawable.order,
        R.drawable.plane,
        R.drawable.train,
        R.drawable.construction,
        R.drawable.container
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hook
        title = getResources().getStringArray(R.array.title);
        desc = getResources().getStringArray(R.array.description);
        recyclerView = findViewById(R.id.recyclerView);

        MyAdapter myAdapter = new MyAdapter(title, desc, images, this);
        recyclerView.setAdapter(myAdapter);

        //LinearLayoutManager
        //recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //GridLayoutManager
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2, RecyclerView.VERTICAL, false));

    }
}