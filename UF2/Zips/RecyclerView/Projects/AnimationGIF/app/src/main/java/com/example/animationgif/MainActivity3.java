package com.example.animationgif;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity3 extends AppCompatActivity {

    private AnimationDrawable animation;
    ImageView imageView;
    TextView textView;
    ObjectAnimator objectAnimator;
    ObjectAnimator objectAnimator2;
    ObjectAnimator objectAnimator3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        animation = new AnimationDrawable();
        imageView = findViewById(R.id.imageView3);
        textView = findViewById(R.id.startText3);

        String sImage = "spaceship";

        for (int i = 1; i < 225; i++){
            animation.addFrame(
                    getResources().getDrawable(
                            getResources().getIdentifier(sImage + i, "drawable",
                                    getPackageName())),20);
        }
        animation.setOneShot(false);
        imageView.setImageDrawable(animation);
        animation.start();

        objectAnimator = ObjectAnimator.ofFloat(textView, "scaleX", 0f, 1f);
        objectAnimator2 = ObjectAnimator.ofFloat(textView, "scaleY", 0f, 1f);
        objectAnimator3 = ObjectAnimator.ofFloat(textView, "alpha", 0f, 1f);
        objectAnimator.setDuration(2000);
        objectAnimator2.setDuration(2000);
        objectAnimator3.setStartDelay(500);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator, objectAnimator2, objectAnimator3);
        animatorSet.start();

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity3.this, MainActivity.class);
                startActivity(i);
            }
        });
    }
}