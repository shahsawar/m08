package com.example.examenuf2;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private Context mContext;
    private List<Soccer> mSoccer;

    public MyAdapter(Context mContext, List<Soccer> mSoccer) {
        this.mContext = mContext;
        this.mSoccer = mSoccer;
    }

    @NonNull
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.soccer_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder holder, int position) {

        Picasso.get().load(mSoccer.get(position).getStrBadge()).fit().centerCrop().into(holder.imageUrl);
        holder.title.setText(mSoccer.get(position).getStrLeague());
        holder.desc.setText(mSoccer.get(position).getStrDescriptionEN());

        holder.buttonWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http:" + mSoccer.get(position).getStrWebSite()));

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }

                mContext.startActivity(i);
            }
        });


        //Ocultar el boton si no hay imagenes
        if (mSoccer.get(position).getStrFanart1() == "null"){
            holder.imagesButton.setVisibility(View.INVISIBLE);
        }


        holder.imagesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, Images.class);
                intent.putExtra("strFanart1", mSoccer.get(position).getStrFanart1());
                intent.putExtra("strFanart2", mSoccer.get(position).getStrFanart2());
                intent.putExtra("strFanart3", mSoccer.get(position).getStrFanart3());
                intent.putExtra("strFanart4", mSoccer.get(position).getStrFanart4());

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }

                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mSoccer.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageUrl;
        TextView title;
        TextView desc;
        Button buttonWeb;
        Button imagesButton;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageUrl = itemView.findViewById(R.id.imageView);
            title = itemView.findViewById(R.id.textTitle);
            desc = itemView.findViewById(R.id.textDescription);
            buttonWeb = itemView.findViewById(R.id.webBtn);
            imagesButton = itemView.findViewById(R.id.imagesBtn);
        }
    }
}
