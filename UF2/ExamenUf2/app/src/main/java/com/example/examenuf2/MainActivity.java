package com.example.examenuf2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hook
        spinner = findViewById(R.id.spinnerArraylist);

        //Spinner
        List<String> countries = new ArrayList<>();
        countries.add(0, "Selecciona curs");
        countries.add("Spain");
        countries.add("England");
        countries.add("France");
        countries.add("Germany");
        countries.add("Italy");
        countries.add("Argentina");
        countries.add("Brazil");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_activated_1,
                countries);

        spinner.setAdapter(dataAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedCondition = parent.getItemAtPosition(position).toString();
                String soccerUrl = "https://www.thesportsdb.com/api/v1/json/1/search_all_leagues.php?c=" + selectedCondition + "&s=Soccer";

                if (selectedCondition != "Selecciona curs") {
                    //Pasar url a otra clase
                    Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                    intent.putExtra("soccerUrl", soccerUrl);
                    startActivity(intent);
                }

                Toast.makeText(MainActivity.this, soccerUrl, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(MainActivity.this, "Nothing selected", Toast.LENGTH_SHORT).show();
            }
        });


    }
}