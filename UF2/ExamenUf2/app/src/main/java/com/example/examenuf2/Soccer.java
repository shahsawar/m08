package com.example.examenuf2;

import java.io.Serializable;

public class Soccer implements Serializable {

    private String strBadge;
    private String strLeague;
    private String strDescriptionEN;
    private String strWebSite;
    private String strFanart1;
    private String strFanart2;
    private String strFanart3;
    private String strFanart4;

    public Soccer(){

    }

    public Soccer(String strBadge, String strLeague, String strDescriptionEN, String strWebSite, String strFanart1, String strFanart2, String strFanart3, String strFanart4) {
        this.strBadge = strBadge;
        this.strLeague = strLeague;
        this.strDescriptionEN = strDescriptionEN;
        this.strWebSite = strWebSite;
        this.strFanart1 = strFanart1;
        this.strFanart2 = strFanart2;
        this.strFanart3 = strFanart3;
        this.strFanart4 = strFanart4;
    }

    public String getStrBadge() {
        return strBadge;
    }

    public void setStrBadge(String strBadge) {
        this.strBadge = strBadge;
    }

    public String getStrLeague() {
        return strLeague;
    }

    public void setStrLeague(String strLeague) {
        this.strLeague = strLeague;
    }

    public String getStrDescriptionEN() {
        return strDescriptionEN;
    }

    public void setStrDescriptionEN(String strDescriptionEN) {
        this.strDescriptionEN = strDescriptionEN;
    }

    public String getStrWebSite() {
        return strWebSite;
    }

    public void setStrWebSite(String strWebSite) {
        this.strWebSite = strWebSite;
    }

    public String getStrFanart1() {
        return strFanart1;
    }

    public void setStrFanart1(String strFanart1) {
        this.strFanart1 = strFanart1;
    }

    public String getStrFanart2() {
        return strFanart2;
    }

    public void setStrFanart2(String strFanart2) {
        this.strFanart2 = strFanart2;
    }

    public String getStrFanart3() {
        return strFanart3;
    }

    public void setStrFanart3(String strFanart3) {
        this.strFanart3 = strFanart3;
    }

    public String getStrFanart4() {
        return strFanart4;
    }

    public void setStrFanart4(String strFanart4) {
        this.strFanart4 = strFanart4;
    }
}
