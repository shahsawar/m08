package com.example.examenuf2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DetailActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    List<Soccer> soccerList = new ArrayList<>();
    MyAdapter myAdapter;
    private static String JSON_URL = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //Hook
        recyclerView = findViewById(R.id.recyclerView);

        //Get country url
        if (getIntent().hasExtra("soccerUrl")) {
            JSON_URL = getIntent().getStringExtra("soccerUrl");
            System.out.println("The json path" + JSON_URL);
        }

        getSoccerData();


    }

    public void getSoccerData() {
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                JSON_URL,
                (String) null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArrayCountry = response.getJSONArray("countrys");

                            for (int i = 0; i < jsonArrayCountry.length(); i++) {
                                JSONObject soccerDetail = jsonArrayCountry.getJSONObject(i);

                                Soccer soccerTmp = new Soccer();
                                soccerTmp.setStrBadge(soccerDetail.getString("strBadge"));
                                soccerTmp.setStrLeague(soccerDetail.getString("strLeague"));
                                soccerTmp.setStrDescriptionEN(soccerDetail.getString("strDescriptionEN"));
                                soccerTmp.setStrWebSite(soccerDetail.getString("strWebsite"));
                                soccerTmp.setStrFanart1(soccerDetail.getString("strFanart1"));
                                soccerTmp.setStrFanart2(soccerDetail.getString("strFanart2"));
                                soccerTmp.setStrFanart3(soccerDetail.getString("strFanart3"));
                                soccerTmp.setStrFanart4(soccerDetail.getString("strFanart4"));
                                soccerList.add(soccerTmp);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        myAdapter = new MyAdapter(getApplicationContext(), soccerList);
                        recyclerView.setAdapter(myAdapter);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("tag", "onErrorResponse: " + error.getMessage());
                    }
                }
        );
        queue.add(jsonObjectRequest);
    }

}