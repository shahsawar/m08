package com.example.examenuf2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import java.util.List;

public class Images extends AppCompatActivity {

    private String[] imageUrls = new String[4];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images);


        //Get images
        if (getIntent().hasExtra("strFanart1")) {
            imageUrls[0] = getIntent().getStringExtra("strFanart1");
            imageUrls[1] = getIntent().getStringExtra("strFanart2");
            imageUrls[2] = getIntent().getStringExtra("strFanart3");
            imageUrls[3] = getIntent().getStringExtra("strFanart4");
        }


        ViewPager viewPager = findViewById(R.id.viewPager);

        ImageAdapter imageAdapter = new ImageAdapter(this, imageUrls);
        viewPager.setAdapter(imageAdapter);
    }
}