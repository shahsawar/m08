package com.example.allactionactivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button webView;
    private Button web;
    private Button map;
    private Button instagram;
    private Button facebook;
    private Button mail;
    private Button call;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webView = findViewById(R.id.webView);
        web = findViewById(R.id.web);
        map = findViewById(R.id.map);
        instagram = findViewById(R.id.instagram);
        facebook = findViewById(R.id.facebook);
        mail = findViewById(R.id.mail);
        call = findViewById(R.id.call);


        webView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, WebViewActivity.class);
                startActivity(i);
            }
        });

        web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = "http://www.escoladeltreball.org";
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(i);

                /*
                String url = "http://www.escoladeltreball.org";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);*/
            }
        });

        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Amb el nom:
                // Map point based on address
                Uri location = Uri.parse("geo:0,0?q=1600+Escola+del+Treball,+Barcelona,+Espanya");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);
                startActivity(mapIntent);

                /*
                //Amb la latitud i longitud:
                // Map point based on address
                Uri location = Uri.parse("geo:41.3890464,2.1454964?z=17");   // z param is zoom levelIntent map
                Intent = new Intent(Intent.ACTION_VIEW, location);
                startActivity(mapIntent);*/
            }
        });

        instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("instagram://edtbarcelona");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {//Si no troba la app Instagram, obrirà interne
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://instagram.com/edtbarcelona")));
                }
            }
        });

        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String urlId = "fb://page/proactivaservice";
                String urlUrl = "https://www.facebook.com/proactivaservice";
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlId)));
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlUrl)));
                }
            }
        });

        mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, MailActivity.class);
                startActivity(i);
            }
        });

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, CallActivity.class);
                startActivity(i);
            }
        });

    }
}