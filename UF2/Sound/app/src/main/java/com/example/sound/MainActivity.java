package com.example.sound;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static int REWIND_TIME = 5000;
    public static int FASTFORWARD_TIME = 5000;
    private static final String PLAY_TIME = "play_time";
    private int mCurrentPosition = 0;

    public static int UPDATE_SEEKBAR = 100;
    Handler handler = new Handler(Looper.getMainLooper());

    private SeekBar seekBarVolume;
    private SeekBar seekBarProgress;
    private Button btnBack;
    private Button btnForward;
    private Button btnPlay;
    private Button btnPause;
    MediaPlayer mediaPlayer;


    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mediaPlayer.isPlaying()){
            mediaPlayer.pause();
        }
        outState.putInt(PLAY_TIME, mediaPlayer.getCurrentPosition());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hook
        seekBarProgress = findViewById(R.id.seekBarProgress);
        seekBarVolume = findViewById(R.id.seekBarVolume);
        btnBack = findViewById(R.id.buttonBack);
        btnForward = findViewById(R.id.buttonForward);
        btnPlay = findViewById(R.id.buttonPlay);
        btnPause = findViewById(R.id.buttonPause);

        mediaPlayer = MediaPlayer.create(this, R.raw.ringtone);

        seekBarProgress.setMax(mediaPlayer.getDuration());

        AudioManager audioManager;
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        seekBarVolume.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        seekBarVolume.setProgress(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));

        if (savedInstanceState != null){
            mCurrentPosition = savedInstanceState.getInt(PLAY_TIME);
            mediaPlayer.seekTo(mCurrentPosition);
        }

        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playAudio();
            }
        });

        btnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pauseAudio();
            }
        });

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                seekBarProgress.setProgress(0);
                mediaPlayer.reset();
                mediaPlayer.release();
                mediaPlayer = null;
                Toast.makeText(MainActivity.this, "Song has finished", Toast.LENGTH_SHORT).show();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rewind();
            }
        });

        btnForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fastForward();
            }
        });

        seekBarProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser){
                    mediaPlayer.seekTo(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekBarVolume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    public void playAudio() {
        //mediaPlayer.start();

        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer.create(this, R.raw.ringtone);
        }
        mediaPlayer.start();

        UpdateSeekBarProgress updateSeekBarProgress = new UpdateSeekBarProgress();
        handler.post(updateSeekBarProgress);
    }

    public void pauseAudio() {
        mediaPlayer.pause();
    }

    @Override
    protected void onStop() {
        releaseMediaPlayer();
        super.onStop();
    }

    private void releaseMediaPlayer() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    private void rewind(){
        int currentPosition = mediaPlayer.getCurrentPosition();
        if (currentPosition - REWIND_TIME > 0){
            mediaPlayer.seekTo(currentPosition - REWIND_TIME);
        }
    }

    private void fastForward(){
        int currentPosition = mediaPlayer.getCurrentPosition();
        int duration = mediaPlayer.getDuration();
        if (currentPosition + FASTFORWARD_TIME < duration){
            mediaPlayer.seekTo(currentPosition + FASTFORWARD_TIME);
        }
    }

    public class UpdateSeekBarProgress implements Runnable{
        @Override
        public void run() {
            if(mediaPlayer!=null){
                seekBarProgress.setProgress(mediaPlayer.getCurrentPosition());
                handler.postDelayed(this, UPDATE_SEEKBAR);
            }
        }
    }
}