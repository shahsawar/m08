package com.example.animationgif;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


public class MainActivity2 extends AppCompatActivity {

    private AnimationDrawable animation;
    ImageView imageView;
    ObjectAnimator objectAnimator;
    ObjectAnimator objectAnimator2;
    ObjectAnimator objectAnimator3;
    TextView start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        animation = new AnimationDrawable();
        imageView = findViewById(R.id.imageView2);
        start = findViewById(R.id.startText);

        String sImage = "rocket";

        for (int i = 17; i < 167; i++){
            animation.addFrame(
                    getResources().getDrawable(
                            getResources().getIdentifier(sImage + i, "drawable",
                                    getPackageName())),20);
        }
        animation.setOneShot(false);
        imageView.setImageDrawable(animation);
        animation.start();

        objectAnimator = ObjectAnimator.ofFloat(start, "scaleX", 0f, 1f);
        objectAnimator2 = ObjectAnimator.ofFloat(start, "scaleY", 0f, 1f);
        objectAnimator3 = ObjectAnimator.ofFloat(start, "alpha", 0f, 1f);
        objectAnimator.setDuration(2000);
        objectAnimator2.setDuration(2000);
        objectAnimator3.setStartDelay(500);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator, objectAnimator2, objectAnimator3);
        animatorSet.start();

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity2.this, MainActivity3.class);
                startActivity(i);
            }
        });
    }
}