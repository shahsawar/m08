package com.example.recyclerviewgridlayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Picture> pictures = new ArrayList<>();
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hook
        recyclerView = findViewById(R.id.recycleView);

        //Add all images in array
        addPictures();

        MyAdapter myAdapter = new MyAdapter(pictures, this);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3, RecyclerView.VERTICAL, false));

    }

    public void addPictures(){
        pictures.add(new Picture("Pic01","https://joanseculi.com/images/img01.jpg", "This is Pic01"));
        pictures.add(new Picture("Pic02","https://joanseculi.com/images/img02.jpg", "This is Pic02"));
        pictures.add(new Picture("Pic03","https://joanseculi.com/images/img22.jpg", "This is Pic03"));
        pictures.add(new Picture("Pic04","https://joanseculi.com/images/img04.jpg", "This is Pic04"));
        pictures.add(new Picture("Pic05","https://joanseculi.com/images/img05.jpg", "This is Pic05"));
        pictures.add(new Picture("Pic06","https://joanseculi.com/images/img06.jpg", "This is Pic06"));
        pictures.add(new Picture("Pic07","https://joanseculi.com/images/img07.jpg", "This is Pic07"));
        pictures.add(new Picture("Pic08","https://joanseculi.com/images/img08.jpg", "This is Pic08"));
        pictures.add(new Picture("Pic09","https://joanseculi.com/images/img09.jpg", "This is Pic09"));
        pictures.add(new Picture("Pic10","https://joanseculi.com/images/img10.jpg", "This is Pic10"));
        pictures.add(new Picture("Pic11","https://joanseculi.com/images/img11.jpg", "This is Pic11"));
        pictures.add(new Picture("Pic12","https://joanseculi.com/images/img12.jpg", "This is Pic12"));
        pictures.add(new Picture("Pic13","https://joanseculi.com/images/img13.jpg", "This is Pic13"));
        pictures.add(new Picture("Pic14","https://joanseculi.com/images/img14.jpg", "This is Pic14"));
        pictures.add(new Picture("Pic15","https://joanseculi.com/images/img15.jpg", "This is Pic15"));
        pictures.add(new Picture("Pic16","https://joanseculi.com/images/img16.jpg", "This is Pic16"));
        pictures.add(new Picture("Pic17","https://joanseculi.com/images/img17.jpg", "This is Pic17"));
        pictures.add(new Picture("Pic18","https://joanseculi.com/images/img18.jpg", "This is Pic18"));
        pictures.add(new Picture("Pic19","https://joanseculi.com/images/img19.jpg", "This is Pic19"));
        pictures.add(new Picture("Pic20","https://joanseculi.com/images/img20.jpg", "This is Pic20"));
        pictures.add(new Picture("Pic21","https://joanseculi.com/images/img21.jpg", "This is Pic21"));
    }
}