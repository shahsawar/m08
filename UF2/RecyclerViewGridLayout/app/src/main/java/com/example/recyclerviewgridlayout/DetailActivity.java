package com.example.recyclerviewgridlayout;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {

    private TextView text;
    private TextView desc;
    private ImageView imageView;

    String getText;
    String getDesc;
    String getImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //Hook
        text = findViewById(R.id.textDetail);
        desc = findViewById(R.id.descDetail);
        imageView = findViewById(R.id.imageDetail);

        getData();
        setData();

    }

    private void getData(){
        if (getIntent().hasExtra("text") && getIntent().hasExtra("desc")){
            getText = getIntent().getStringExtra("text");
            getDesc = getIntent().getStringExtra("desc");
            getImage = getIntent().getStringExtra("url");
        }else {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData(){
        text.setText(getText);
        desc.setText(getDesc);
        Picasso.get().load(getImage).into(imageView);
    }
}