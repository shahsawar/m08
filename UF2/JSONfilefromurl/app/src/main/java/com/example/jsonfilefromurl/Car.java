package com.example.jsonfilefromurl;


import java.io.Serializable;

public class Car implements Serializable {
    private int year;
    private String make;
    private String model;
    private String category;
    private String url;
    private String createAt;
    private String updateAt;


    public Car() {
    }

    public Car(int year, String make, String model, String category, String url, String createAt, String updateAt) {
        this.year = year;
        this.make = make;
        this.model = model;
        this.category = category;
        this.url = url;
        this.createAt = createAt;
        this.updateAt = updateAt;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }
}
