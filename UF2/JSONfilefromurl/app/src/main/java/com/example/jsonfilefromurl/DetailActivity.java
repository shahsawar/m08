package com.example.jsonfilefromurl;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

public class DetailActivity extends AppCompatActivity {

    private TextView textYear;
    private TextView textMake;
    private TextView textModel;
    private ImageView imageView;
    private TextView textCategory;
    private TextView textCreatedAt;
    private TextView textUpdatedAt;
    Car carTmp;

    int year;
    String make, model, imageUrl, category, createdAt, updatedAt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //Hook
        textYear = findViewById(R.id.fieldYear);
        textMake = findViewById(R.id.fieldMake);
        textModel = findViewById(R.id.fieldModle);
        imageView = findViewById(R.id.imageView);
        textCategory = findViewById(R.id.fieldCategory);
        textCreatedAt = findViewById(R.id.fieldCreatedAt);
        textUpdatedAt = findViewById(R.id.fieldUpdatedAt);

        getData();
        setData();

    }

    private void getData(){

        //Recibo el objeto car
        carTmp = (Car) getIntent().getSerializableExtra("car");

        /*
        if (getIntent().hasExtra("make") && getIntent().hasExtra("model")){
            year = getIntent().getIntExtra("year", 0);
            make = getIntent().getStringExtra("make");
            model = getIntent().getStringExtra("model");
            imageUrl = getIntent().getStringExtra("url");
            category = getIntent().getStringExtra("category");
            createdAt = getIntent().getStringExtra("createdAt");
            updatedAt = getIntent().getStringExtra("updatedAt");
        }*/
    }

    private void setData(){
        textYear.setText(String.valueOf(carTmp.getYear()));
        textMake.setText(carTmp.getMake());
        textModel.setText(carTmp.getModel());
        textCategory.setText(carTmp.getCategory());
        textCreatedAt.setText(carTmp.getCreateAt());
        textUpdatedAt.setText(carTmp.getUpdateAt());
        Picasso.get().load(carTmp.getUrl()).into(imageView);
    }
}