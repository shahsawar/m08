package com.example.jsonfilefromassertsedt37;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private Context mContext;
    private List<String> peakNames;
    private List<String> peakHeight;
    private List<String> peakUrl;
    private List<String> peakCountry;

    //Constructor
    public MyAdapter(Context mContext, List<String> peakNames, List<String> peakHeight, List<String> peakUrl, List<String> peakCountry) {
        this.mContext = mContext;
        this.peakNames = peakNames;
        this.peakHeight = peakHeight;
        this.peakUrl = peakUrl;
        this.peakCountry = peakCountry;
    }

    @NonNull
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.peak_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder holder, int position) {
        Picasso.get().load(peakUrl.get(position)).fit().centerCrop().into(holder.imageUrl);
        holder.textName.setText(peakNames.get(position));
        holder.textHeight.setText(peakHeight.get(position));
        holder.textCountry.setText(peakCountry.get(position));
    }

    @Override
    public int getItemCount() {
        return peakNames.size();
    }

    //Inner class
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textName;
        TextView textHeight;
        ImageView imageUrl;
        TextView textCountry;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            textName = itemView.findViewById(R.id.textName);
            textHeight = itemView.findViewById(R.id.textHeight);
            imageUrl = itemView.findViewById(R.id.imageUrl);
            textCountry = itemView.findViewById(R.id.textCountry);
        }
    }

}
