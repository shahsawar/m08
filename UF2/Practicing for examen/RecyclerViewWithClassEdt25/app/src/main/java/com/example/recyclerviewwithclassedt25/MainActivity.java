package com.example.recyclerviewwithclassedt25;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Animal> animals = new ArrayList<>();
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);
        initData();

        MyAdapter myAdapter = new MyAdapter(animals, this);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void initData(){
        String[] animalMeals = {"M01", "M02"};
        Animal animal1 = new Animal("https://i.picsum.photos/id/1003/1181/1772.jpg?hmac=oN9fHMXiqe9Zq2RM6XT-RVZkojgPnECWwyEF1RvvTZk", "Deer", "Peter Pan", 7, 2013, animalMeals, "Deer or true deer are hoofed ruminant mammals forming the family Cervidae. The two main groups of deer are the Cervinae, including the muntjac, the elk, the red deer, the fallow deer, and the chital; and the Capreolinae, including the reindeer, the roe deer, the mule deer, and the moose.");
        animals.add(animal1);

        String[] animalMeals2 = {"M03", "M05", "M07"};
        Animal animal2 = new Animal("https://i.picsum.photos/id/1084/4579/3271.jpg?hmac=YblMazviSugJVfZsFPaFI_Vp6lBeQin62qpm8rxHruo", "Seal", "Guetta", 15, 2005, animalMeals2, "Pinnipeds, commonly known as seals, are a widely distributed and diverse clade of carnivorous, fin-footed, semiaquatic marine mammals. They comprise the extant families Odobenidae, Otariidae, and Phocidae. There are 33 extant species of pinnipeds, and more than 50 extinct species have been described from fossils.");
        animals.add(animal2);

        String[] animalMeals3 = {"M03", "M07", "M08"};
        Animal animal3 = new Animal("https://i.picsum.photos/id/1069/3500/2333.jpg?hmac=VBJ1vR2opkcKLS9NKGDl5uPxF02u6dSqbwc1x1b4oJc", "Medusa", "Vegas", 6, 2014, animalMeals3, "Medusozoa is a clade in the phylum Cnidaria, and is often considered a subphylum. It includes the classes Hydrozoa, Scyphozoa, Staurozoa and Cubozoa, and possibly the parasitic Polypodiozoa.");
        animals.add(animal3);

        String[] animalMeals4 = {"M03", "M08"};
        Animal animal4 = new Animal("https://i.picsum.photos/id/1074/5472/3648.jpg?hmac=w-Fbv9bl0KpEUgZugbsiGk3Y2-LGAuiLZOYsRk0zo4A", "Lioness", "Punk", 11, 2009, animalMeals4, "The lion is a species in the family Felidae and a member of the genus Panthera. It has a muscular, deep-chested body, short, rounded head, round ears, and a hairy tuft at the end of its tail. It is sexually dimorphic; adult male lions have a prominent mane.");
        animals.add(animal4);

        String[] animalMeals5 = {"M03", "M05"};
        Animal animal5 = new Animal("https://i.picsum.photos/id/219/5184/3456.jpg?hmac=2LU7i3c6fykd_J0T6rZm1aBoBmK4ivkH1Oc459aRUU0", "Tiger", "Garrix", 5, 2015, animalMeals5, "The tiger is the largest extant cat species and a member of the genus Panthera. It is most recognisable for its dark vertical stripes on orange-brown fur with a lighter underside. It is an apex predator, primarily preying on ungulates such as deer and wild boar.");
        animals.add(animal5);

        String[] animalMeals6 = {"M03", "M08"};
        Animal animal6 = new Animal("https://i.picsum.photos/id/433/4752/3168.jpg?hmac=Og-twcmaH_j-JNExl5FsJk1pFA7o3-F0qeOblQiJm4s", "Bear", "Walker", 35, 1985, animalMeals6, "Bears are carnivoran mammals of the family Ursidae. They are classified as caniforms, or doglike carnivorans. Although only eight species of bears are extant, they are widespread, appearing in a wide variety of habitats throughout the Northern Hemisphere and partially in the Southern Hemisphere.");
        animals.add(animal6);

        String[] animalMeals7 = {"M07", "M08"};
        Animal animal7 = new Animal("https://i.picsum.photos/id/943/3000/2002.jpg?hmac=GbwwAGwHcOJ5HL17XLMbHLRrVnLuTzadxw43YSktrmA", "Reindeer", "Marshmello", 25, 1995, animalMeals7, "The reindeer, also known as caribou in North America, is a species of deer with circumpolar distribution, native to Arctic, sub-Arctic, tundra, boreal, and mountainous regions of northern Europe, Siberia, and North America. This includes both sedentary and migratory populations.");
        animals.add(animal7);
    }
}