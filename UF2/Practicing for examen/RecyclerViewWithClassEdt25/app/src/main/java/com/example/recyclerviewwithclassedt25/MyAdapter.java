package com.example.recyclerviewwithclassedt25;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewAdapter> {

    private ArrayList<Animal> animals;
    private Context context;

    public MyAdapter(ArrayList<Animal> animals, Context context) {
        this.animals = animals;
        this.context = context;
    }

    @NonNull
    @Override
    public MyAdapter.MyViewAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.animal_row, parent, false);
        return new MyViewAdapter(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyViewAdapter holder, int position) {
        //Animal image
        Picasso.get().load(animals.get(position).getUrlImage()).fit().centerCrop().into(holder.imageView);

        //Animal info
        holder.textType.setText(animals.get(position).getType());
        holder.textNickname.setText(animals.get(position).getNickname());
        holder.textAge.setText(String.valueOf(animals.get(position).getAge()));
        holder.textBirthYear.setText(String.valueOf(animals.get(position).getBirthYear()));
        holder.textMeals.setText(animals.get(position).getMealsAnimal());

        holder.rowLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailActivity.class);

                /*
                intent.putExtra("type", animals.get(position).getType());
                intent.putExtra("nickname", animals.get(position).getNickname());
                intent.putExtra("age", animals.get(position).getAge());
                intent.putExtra("birthyear", animals.get(position).getBirthYear());
                intent.putExtra("meals", animals.get(position).getMealsAnimal());
                intent.putExtra("desc", animals.get(position).getDesc());

                intent.putExtra("urlImage", animals.get(position).getUrlImage());
                */

                intent.putExtra("animal", animals.get(position));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return animals.size();
    }

    public class MyViewAdapter extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private TextView textType;
        private TextView textNickname;
        private TextView textAge;
        private TextView textBirthYear;
        private TextView textMeals;

        ConstraintLayout rowLayout;

        public MyViewAdapter(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            textType = itemView.findViewById(R.id.type);
            textNickname = itemView.findViewById(R.id.textNickname);
            textAge = itemView.findViewById(R.id.textAge);
            textBirthYear = itemView.findViewById(R.id.textBirthday);
            textMeals = itemView.findViewById(R.id.textMeals);
            rowLayout = itemView.findViewById(R.id.myRow);
        }
    }
}
