package com.example.recyclerviewwithclassedt25;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {

    private TextView textType;
    private TextView textNickname;
    private TextView textAge;
    private TextView textBirthYear;
    private TextView textMeals;
    private TextView textDesc;
    private ImageView imageDetail;

    String type, nickname, meals, desc, urlImage;
    int age, birthYear;

    Animal animalTmp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        textType = findViewById(R.id.textType);
        textNickname = findViewById(R.id.textNickname);
        textAge = findViewById(R.id.textAge);
        textBirthYear = findViewById(R.id.textBirthday);
        textMeals = findViewById(R.id.textMeals);
        textDesc = findViewById(R.id.textDesc);
        imageDetail = findViewById(R.id.animalImage);

        getData();
        setData();

    }

    private void getData() {

        if (getIntent().getSerializableExtra("animal") != null) {
            animalTmp = (Animal) getIntent().getSerializableExtra("animal");
        } else {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }

        /*
        if (getIntent().hasExtra("type") && getIntent().hasExtra("nickname")) {

            type = getIntent().getStringExtra("type");
            nickname = getIntent().getStringExtra("nickname");
            age = getIntent().getIntExtra("age", 1);
            birthYear = getIntent().getIntExtra("birthyear", 1);
            meals = getIntent().getStringExtra("meals");
            desc = getIntent().getStringExtra("desc");
            urlImage = getIntent().getStringExtra("urlImage");
        } else {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }*/
    }

    public void setData() {

        textType.setText(animalTmp.getType());
        textNickname.setText(animalTmp.getNickname());
        textAge.setText(animalTmp.getAge() + "");
        textBirthYear.setText(animalTmp.getBirthYear() + "");
        textMeals.setText(animalTmp.getMealsAnimal());
        textDesc.setText(animalTmp.getDesc());
        Picasso.get().load(animalTmp.getUrlImage()).into(imageDetail);

        /*
        textType.setText(type);
        textNickname.setText(nickname);
        textAge.setText(age + "");
        textBirthYear.setText(birthYear + "");
        textMeals.setText(meals);
        textDesc.setText(desc);
        Picasso.get().load(urlImage).into(imageDetail);*/
    }
}
