package com.example.jsonfilefromurl_edt38;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private Context mContext;
    private List<Car> mCar;

    public MyAdapter(Context mContext, List<Car> mCar) {
        this.mContext = mContext;
        this.mCar = mCar;
    }

    @NonNull
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.car_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder holder, int position) {
        holder.make.setText(mCar.get(position).getMake());
        holder.model.setText(mCar.get(position).getModel());
        holder.category.setText(mCar.get(position).getCategory());
        Picasso.get().load(mCar.get(position).getUrl()).fit().centerCrop().into(holder.url);

        holder.carLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Pasar el objecto actual a intent
                Intent intent = new Intent(mContext, DetailActivity.class);
                intent.putExtra("car", mCar.get(position));

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P){
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCar.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView make;
        TextView model;
        TextView category;
        ImageView url;

        //Para activity detail
        ConstraintLayout carLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            make = itemView.findViewById(R.id.textMake);
            model = itemView.findViewById(R.id.textModel);
            category = itemView.findViewById(R.id.textCategory);
            url = itemView.findViewById(R.id.imageViewCar);

            //Para Activity detail
            carLayout = itemView.findViewById(R.id.myCarLayout);
        }
    }
}
