package com.example.recyclerviewedt12b;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private String title[];
    private String desc[];
    private int images[];
    private Context context;

    public MyAdapter(String[] title, String[] desc, int[] images, Context context) {
        this.title = title;
        this.desc = desc;
        this.images = images;
        this.context = context;
    }

    @NonNull
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.my_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder holder, int position) {
        holder.myTextViewRow.setText(title[position]);
        holder.myTextDescRow.setText(desc[position]);
        holder.myImageView.setImageResource(images[position]);
    }

    @Override
    public int getItemCount() {
        return title.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView myTextViewRow;
        TextView myTextDescRow;
        ImageView myImageView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            myTextViewRow = itemView.findViewById(R.id.textTitleRow);
            myTextDescRow = itemView.findViewById(R.id.textDescRow);
            myImageView = itemView.findViewById(R.id.imageView);
        }
    }
}
