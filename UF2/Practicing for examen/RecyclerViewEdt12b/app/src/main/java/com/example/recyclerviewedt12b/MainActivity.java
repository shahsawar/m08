package com.example.recyclerviewedt12b;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    String title[];
    String desc[];
    int images[] = {
            R.drawable.bike,
            R.drawable.boat,
            R.drawable.car,
            R.drawable.construction,
            R.drawable.container,
            R.drawable.order,
            R.drawable.plane,
            R.drawable.train,
            R.drawable.construction,
            R.drawable.container
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hook
        recyclerView = findViewById(R.id.recyclerView);
        title = getResources().getStringArray(R.array.title);
        desc = getResources().getStringArray(R.array.description);

        MyAdapter myAdapter = new MyAdapter(title, desc, images, this);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


    }
}