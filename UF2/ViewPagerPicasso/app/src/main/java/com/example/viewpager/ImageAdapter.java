package com.example.viewpager;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.squareup.picasso.Picasso;

public class ImageAdapter extends PagerAdapter {
    private Context context;
    private String[] mImagesIds;

    public ImageAdapter(Context context, String[] mImagesIds) {
        this.context = context;
        this.mImagesIds = mImagesIds;
    }

    @Override
    public int getCount() {
        return mImagesIds.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        //return false;
        return view.equals(object);
        //return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //super.destroyItem(container, position, object);
        container.removeView((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        //return super.instantiateItem(container, position);
        ImageView imageView = new ImageView(context);
        Picasso.get().load(mImagesIds[position]).fit().centerInside().into(imageView);
        container.addView(imageView);
        return imageView;
    }

    /*
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        //return super.instantiateItem(container, position);
        ImageView imageView = new ImageView(context);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageResource(mImagesIds[position]);
        container.addView(imageView, 0);
        return imageView;
    }*/

}
