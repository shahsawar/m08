package com.example.viewpager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private int[] mImagesIds = new int[]{
            R.drawable.pic1,
            R.drawable.pic2,
            R.drawable.pic3,
            R.drawable.pic4,
            R.drawable.pic5,
            R.drawable.pic6
    };

    private String[] imageUrls = new String[]{
            "https://www.joanseculi.com/images/image01.jpg",
            "https://www.joanseculi.com/images/image02.jpg",
            "https://www.joanseculi.com/images/image03.jpg",
            "https://www.joanseculi.com/images/image04.jpg",
            "https://www.joanseculi.com/images/image05.jpg"
    };


    private TextView textRight;
    private TextView textLeft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ViewPager viewPager = findViewById(R.id.viewPager);

        textLeft = findViewById(R.id.textViewBack);
        textRight = findViewById(R.id.textViewNext);

        ImageAdapter imageAdapter = new ImageAdapter(this, imageUrls);
        viewPager.setAdapter(imageAdapter);

        textRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.arrowScroll(View.FOCUS_RIGHT);
            }
        });

        textLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.arrowScroll(View.FOCUS_LEFT);
            }
        });
    }
}