package com.example.spinnerv2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Spinner spinnerString;
    Spinner spinnerPerJava;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hook
        spinnerString = findViewById(R.id.spinnerString);
        spinnerPerJava = findViewById(R.id.spinnerArraylist);

        //Si no fem servir strings.xml
        List<String> categories = new ArrayList<>();
        categories.add(0, "Selecciona curs");
        categories.add("1DAM");
        categories.add("2DAM");
        categories.add("1DAW");
        categories.add("2DAW");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_activated_1,
                categories);

        spinnerPerJava.setAdapter(dataAdapter);

        spinnerPerJava.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedCondition = parent.getItemAtPosition(position).toString();
                Toast.makeText(MainActivity.this,selectedCondition, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerString.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedCondition = parent.getItemAtPosition(position).toString();
                Toast.makeText(MainActivity.this,selectedCondition, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(MainActivity.this,"Nothing selected", Toast.LENGTH_SHORT).show();
            }
        });
    }
}