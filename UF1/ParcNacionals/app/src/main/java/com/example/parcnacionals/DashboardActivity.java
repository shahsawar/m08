package com.example.parcnacionals;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class DashboardActivity extends AppCompatActivity {

    public static String PARK_IMAGE = "com.example.parcnacionals.PARK_IMAGE";

    CardView cardView1;
    CardView cardView2;
    CardView cardView3;
    CardView cardView4;
    CardView cardView5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);


        //Hook
        cardView1 = findViewById(R.id.cardView1);
        cardView2 = findViewById(R.id.cardView2);
        cardView3 = findViewById(R.id.cardView3);
        cardView4 = findViewById(R.id.cardView4);
        cardView5 = findViewById(R.id.cardView5);

        cardView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int img = R.drawable.foto1;
                //Intent
                Intent intent = new Intent(DashboardActivity.this, ReActivity.class);
                intent.putExtra(PARK_IMAGE, img);
                startActivity(intent);
            }
        });

        cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int img = R.drawable.foto2;
                //Intent
                Intent intent = new Intent(DashboardActivity.this, ReActivity.class);
                intent.putExtra(PARK_IMAGE, img);
                startActivity(intent);
            }
        });

        cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int img = R.drawable.foto3;
                //Intent
                Intent intent = new Intent(DashboardActivity.this, ReActivity.class);
                intent.putExtra(PARK_IMAGE, img);
                startActivity(intent);
            }
        });

        cardView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int img = R.drawable.foto4;
                //Intent
                Intent intent = new Intent(DashboardActivity.this, ReActivity.class);
                intent.putExtra(PARK_IMAGE, img);
                startActivity(intent);
            }
        });

        cardView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int img = R.drawable.foto5;
                //Intent
                Intent intent = new Intent(DashboardActivity.this, ReActivity.class);
                intent.putExtra(PARK_IMAGE, img);
                startActivity(intent);
            }
        });

    }
}