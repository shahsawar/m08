package com.example.parcnacionals;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ReActivity extends AppCompatActivity {

    private ImageView imageView;
    private RadioGroup radioGroup1;
    private RadioButton radioBt1;
    private RadioButton radioBt2;
    private RadioButton radioBt3;
    private RadioButton radioBt4;
    private Switch switchBt;

    private DatePickerDialog picker;
    private EditText textDateEn;
    private EditText textDateSo;

    Spinner spinnerPerJava;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserva);


        //Hook
        radioGroup1 = findViewById(R.id.radioGroup1);
        radioBt1 = findViewById(R.id.radioB1);
        radioBt2 = findViewById(R.id.radioB2);
        radioBt3 = findViewById(R.id.radioB3);
        radioBt4 = findViewById(R.id.radioB4);
        switchBt = findViewById(R.id.switch1);
        textDateEn = findViewById(R.id.dateEntrada);
        textDateSo = findViewById(R.id.dateSortida);
        spinnerPerJava = findViewById(R.id.spinnerPerJava);
        imageView = findViewById(R.id.resImg);

        Intent i = getIntent();
        int imgTitle = i.getIntExtra(DashboardActivity.PARK_IMAGE,0);
        imageView.setImageResource(imgTitle);


        //Arraylist
        List categories = new ArrayList<>();
        categories.add(0, "Selecciona Refugi");
        categories.add("Refugi Josep Maria Blanc");
        categories.add("Refugi Cap de Llauset");
        categories.add("Refugi Ventosa i Clavell");
        categories.add("Refugi Amitges");
        categories.add("Refugi Josep Maria Montfort");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_activated_1,
                categories);
        spinnerPerJava.setAdapter(dataAdapter);


        spinnerPerJava.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedCondition = parent.getItemAtPosition(position).toString();
                Toast.makeText(ReActivity.this, selectedCondition, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        textDateEn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar calendar = Calendar.getInstance();
                final int day = calendar.get(Calendar.DAY_OF_MONTH);
                final int month = calendar.get(Calendar.MONDAY);
                final int year = calendar.get(Calendar.YEAR);

                picker = new DatePickerDialog(ReActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                textDateEn.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });

        textDateSo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar calendar = Calendar.getInstance();
                final int day = calendar.get(Calendar.DAY_OF_MONTH);
                final int month = calendar.get(Calendar.MONDAY);
                final int year = calendar.get(Calendar.YEAR);

                picker = new DatePickerDialog(ReActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                textDateSo.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });

        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selected = radioGroup1.getCheckedRadioButtonId();
                switch (selected){
                    case R.id.radioB1:
                        //Toast
                        Toast.makeText(ReActivity.this, "Vegeterià", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioB2:
                        //Toast
                        Toast.makeText(ReActivity.this, "Sense Gluten", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioB3:
                        //Toast
                        Toast.makeText(ReActivity.this, "Celíac", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioB4:
                        //Toast
                        Toast.makeText(ReActivity.this, "Cap", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }
            }
        });
        disableRadioGroup1();
        if(switchBt != null){
            switchBt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        enableRadioGroup1();
                    } else {
                        disableRadioGroup1();
                    }
                }
            });
        }

    }
    public void enableRadioGroup1(){
        for (int i = 0; i < radioGroup1.getChildCount(); i++) {
            radioGroup1.getChildAt(i).setClickable(true);
        }
    }
    public void disableRadioGroup1() {
        for (int i = 0; i < radioGroup1.getChildCount(); i++) {
            radioGroup1.getChildAt(i).setClickable(false);
        }
        radioGroup1.clearCheck();
        radioBt4.setChecked(true);
    };




}