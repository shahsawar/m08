package com.example.parcnacionals;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final int SPLASH_SCREEN = 2000;

    private ImageView logo1;
    private ImageView logo2;
    private TextView titleLogo;
    private TextView subTitle;

    ObjectAnimator objectAnimatorlogo1;
    ObjectAnimator objectAnimatorlogo1_2;
    ObjectAnimator objectAnimatorlogo2;
    ObjectAnimator objectAnimatorTitol;
    ObjectAnimator objectAnimatorTitol2;
    ObjectAnimator objectAnimatorSubtitol;
    ObjectAnimator objectAnimatorSubtitol2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hook
        logo1 = findViewById(R.id.logo1);
        logo2 = findViewById(R.id.logo2);
        titleLogo = findViewById(R.id.logoTitle);
        subTitle = findViewById(R.id.subTitle);

        //Logos
        objectAnimatorlogo1 = ObjectAnimator.ofFloat(logo1, "x", -1000f, 0f);
        objectAnimatorlogo1.setDuration(1000);
        objectAnimatorlogo1_2 = ObjectAnimator.ofFloat(logo1, "alpha", 0.1f, 1f);
        objectAnimatorlogo1_2.setDuration(2000);

        objectAnimatorlogo2 = ObjectAnimator.ofFloat(logo2, "alpha", 0f, 1f);
        objectAnimatorlogo2.setDuration(4000);

        //Images
        objectAnimatorTitol = ObjectAnimator.ofFloat(titleLogo, "x", 1000f, 0f);
        objectAnimatorTitol.setDuration(1000);
        objectAnimatorTitol2 = ObjectAnimator.ofFloat(titleLogo, "alpha", 0.1f, 1f);
        objectAnimatorTitol2.setDuration(2000);

        objectAnimatorSubtitol = ObjectAnimator.ofFloat(subTitle, "x", 1000f, 0f);
        objectAnimatorSubtitol.setDuration(1000);
        objectAnimatorSubtitol2 = ObjectAnimator.ofFloat(subTitle, "alpha", 0.1f, 1f);
        objectAnimatorSubtitol2.setDuration(2000);


        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimatorlogo1,
                objectAnimatorlogo1_2,
                objectAnimatorlogo2,
                objectAnimatorTitol,
                objectAnimatorSubtitol,
                objectAnimatorTitol2,
                objectAnimatorSubtitol2);
        animatorSet.start();



        Runnable r = new Runnable() {
            @Override
            public void run() {
                Pair[] pairs = new Pair[2];
                pairs[0] = new Pair<View,String>(logo1, "logo_origen");
                pairs[1] = new Pair<View,String>(titleLogo, "text_origen");

                //intent
                Intent intent = new Intent(MainActivity.this, DashboardActivity.class);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(
                            MainActivity.this, pairs);
                    startActivity(intent, options.toBundle());
                }
            }
        };
        //Handler h = new Handler(Looper.myLooper());
        Handler h = new Handler();
        h.postDelayed(r, SPLASH_SCREEN);

    }
}