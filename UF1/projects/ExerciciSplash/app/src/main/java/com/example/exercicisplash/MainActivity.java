package com.example.exercicisplash;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView textViewTitle;
    private TextView textViewSubTitle;
    private static final int SPLASH_SCREEN = 2000;
    /*
    ObjectAnimator objectAnimatorImage;
    ObjectAnimator objectAnimatorImage2;
    ObjectAnimator objectAnimatorText;
    ObjectAnimator objectAnimatorText2;
    ObjectAnimator objectAnimatorTextSub;
    ObjectAnimator objectAnimatorTextSub2;*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hook
        imageView = findViewById(R.id.imageView);
        textViewTitle = findViewById(R.id.textTitle);
        textViewSubTitle = findViewById(R.id.textSubTitle);

        animator(imageView, "rotation", 0, 360);
        animator(imageView, "Y", -600, 100);
        animator(textViewTitle, "rotation", 0, 360);
        animator(textViewTitle, "Y", 2000, 1400);
        animator(textViewSubTitle, "rotation", 360, 0);
        animator(textViewSubTitle, "Y", 2000, 1550);

        /*
        objectAnimatorImage = ObjectAnimator.ofFloat(imageView, "rotation", 0f, 360f);
        objectAnimatorImage2 = ObjectAnimator.ofFloat(imageView, "Y", -600f, 100f);
        objectAnimatorText = ObjectAnimator.ofFloat(textViewTitle, "rotation", 0f, 360f);
        objectAnimatorText2 = ObjectAnimator.ofFloat(textViewTitle, "Y", 2000f, 1400f);
        objectAnimatorTextSub = ObjectAnimator.ofFloat(textViewSubTitle, "rotation", 360f, 0f);
        objectAnimatorTextSub2 = ObjectAnimator.ofFloat(textViewSubTitle, "Y", 2000f, 1550f);

        objectAnimatorImage.setDuration(SPLASH_SCREEN);
        objectAnimatorImage2.setDuration(SPLASH_SCREEN);
        objectAnimatorText.setDuration(SPLASH_SCREEN);
        objectAnimatorText2.setDuration(SPLASH_SCREEN);
        objectAnimatorTextSub.setDuration(SPLASH_SCREEN);
        objectAnimatorTextSub2.setDuration(SPLASH_SCREEN);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimatorImage, objectAnimatorImage2, objectAnimatorText, objectAnimatorText2, objectAnimatorTextSub, objectAnimatorTextSub2);
        animatorSet.start();*/


        Runnable r = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, DashboardActivity.class);
                Pair[] pairs = new Pair[2];
                pairs[0] = new Pair<View, String>(imageView, "image_origen");
                pairs[1] = new Pair<View, String>(textViewTitle, "textapp_origen");
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, pairs);
                    startActivity(intent, options.toBundle());
                }
            }
        };
        Handler h = new Handler();
        h.postDelayed(r, SPLASH_SCREEN);
    }

    //Object animator for image
    public void animator(ImageView imageView, String rotation, int from, int to){
        ObjectAnimator objectAnimatorImage;
        objectAnimatorImage = ObjectAnimator.ofFloat(imageView, rotation, from, to);
        objectAnimatorImage.setDuration(SPLASH_SCREEN);
        objectAnimatorImage.start();
    }

    //Object animator for text
    public void animator(TextView textView, String rotation, int from, int to){
        ObjectAnimator objectAnimatorImage;
        objectAnimatorImage = ObjectAnimator.ofFloat(textView, rotation, from, to);
        objectAnimatorImage.setDuration(SPLASH_SCREEN);
        objectAnimatorImage.start();
    }
}