package com.example.exercicisplash;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

public class SignupActivity extends AppCompatActivity {

    private DatePickerDialog picker;
    private EditText textDate;
    private Spinner spinnerClass;
    private ArrayList<Bike> mBike;
    private BikeAdapter mAdapter;
    private TextView userRegisted;
    private Switch switchNewsletter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        //Hook
        textDate = findViewById(R.id.textDate);
        spinnerClass = findViewById(R.id.spinner);
        userRegisted = findViewById(R.id.userRegistered);
        switchNewsletter = findViewById(R.id.switch2);

        textDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar calendar = Calendar.getInstance();
                final int day = calendar.get(Calendar.DAY_OF_MONTH);
                final int month = calendar.get(Calendar.MONDAY);
                final int year = calendar.get(Calendar.YEAR);

                picker = new DatePickerDialog(SignupActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                textDate.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });

        //Spinner Class
        mBike = new ArrayList<>();
        mBike.add(new Bike("Mountain Bike",R.drawable.bici1));
        mBike.add(new Bike("Cycling Bike",R.drawable.bici2));
        mBike.add(new Bike("Electric Bike",R.drawable.bici3));
        mBike.add(new Bike("Road Bike",R.drawable.bici4));
        mBike.add(new Bike("Folding Bike",R.drawable.bici5));
        mBike.add(new Bike("Hybrid Bike",R.drawable.bici6));
        mBike.add(new Bike("Touring Bike",R.drawable.bici7));

        mAdapter = new BikeAdapter(this, mBike);
        spinnerClass.setAdapter(mAdapter);

        spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Bike selectedItem = (Bike) parent.getItemAtPosition(position);
                String selectedDavidBowie = selectedItem.getCoverName();
                Toast.makeText(SignupActivity.this, selectedDavidBowie, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //Toast
                Toast.makeText(SignupActivity.this, "Nothing Selected", Toast.LENGTH_SHORT).show();
            }
        });

        userRegisted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignupActivity.this, DashboardActivity.class);
                startActivity(intent);
            }
        });

        if (switchNewsletter != null){
            switchNewsletter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        Toast.makeText(SignupActivity.this, "" + isChecked, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SignupActivity.this, "" + isChecked, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}