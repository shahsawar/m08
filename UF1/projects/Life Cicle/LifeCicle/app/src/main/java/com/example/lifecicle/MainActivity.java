package com.example.lifecicle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //Time delay
    private static int SPLASH_SCREEN = 1000;
    private static final String HOME_TAG = MainActivity.class.getSimpleName();

    public void toastMsg(String m){
        Toast.makeText(MainActivity.this, m, Toast.LENGTH_SHORT).show();
    }

    public void logMsg(String l){
        Log.i(HOME_TAG, l);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toastMsg("onCreate method called");
        logMsg("Activity Created");
    }

    @Override
    protected void onStart() {
        super.onStart();
        toastMsg("onStart method called");
        logMsg("Activity Started");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        toastMsg("onRestart method called");
        logMsg("Activity Restarted");
    }

    @Override
    protected void onResume() {
        super.onResume();
        toastMsg("onResume method called");
        logMsg("Activity Resumed");
    }

    @Override
    protected void onPause() {
        super.onPause();
        toastMsg("onPause method called");
        logMsg("Activity Paused");
    }

    @Override
    protected void onStop() {
        super.onStop();
        toastMsg("onStop method called");
        logMsg("Activity Stopped");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        toastMsg("onDestroy method called");
        logMsg("Activity is being Destroyed");
    }

    public void button (View view){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                startActivity(intent);
                finish();
            }
        }, SPLASH_SCREEN);
    }
}