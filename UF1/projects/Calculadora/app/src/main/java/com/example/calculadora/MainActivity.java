package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //Time delay
    private static int SPLASH_SCREEN = 1000;
    private static final String HOME_TAG =  MainActivity.class.getSimpleName();

    private EditText num1;
    private EditText num2;
    private TextView textResult;
    private Button btAdd;
    private Button btSubs;
    private Button btMult;
    private Button btDiv;
    private Button btPow;
    private Button btCe;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hook
        textResult = (TextView) findViewById(R.id.textResult);
        num1 = (EditText) findViewById(R.id.num1);
        num2 = (EditText) findViewById(R.id.num2);
        btAdd = (Button) findViewById(R.id.btAdd);
        btSubs = (Button) findViewById(R.id.btSubs);
        btMult = (Button) findViewById(R.id.btMult);
        btDiv = (Button) findViewById(R.id.btDiv);
        btPow = (Button) findViewById(R.id.btPow);
        btCe = (Button) findViewById(R.id.btCe);


        //Suma
        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() ||
                        num2.getText().toString().trim().isEmpty()){
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                    Log.w(HOME_TAG, "Missatge warning");
                }
                else {
                    double result = Double.parseDouble(num1.getText().toString().trim())
                            + Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(""+ result);
                }
            }
        });


        //Resta
        btSubs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() ||
                        num2.getText().toString().trim().isEmpty()){
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                }
                else {
                    double result = Double.parseDouble(num1.getText().toString().trim())
                            - Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(""+ result);
                }
            }
        });

        //Multiplicar
        btMult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() ||
                        num2.getText().toString().trim().isEmpty()){
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                }
                else {
                    double result = Double.parseDouble(num1.getText().toString().trim())
                            * Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(""+ result);
                }
            }
        });

        //dividir
        btDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() ||
                        num2.getText().toString().trim().isEmpty()){
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                }
                else {
                    if (num2.getText().toString().trim().equals("0")){
                        Toast.makeText(MainActivity.this, "Divide by zero impossible", Toast.LENGTH_SHORT).show();
                        textResult.setText("0.0");
                        Log.e(HOME_TAG, num2.getText().toString() + " Divide by zero impossible");
                    }
                    else {
                        double result = Double.parseDouble(num1.getText().toString().trim())
                                / Double.parseDouble(num2.getText().toString().trim());
                        textResult.setText("" + result);
                    }
                }
            }
        });

        //Potencia
        btPow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() ||
                        num2.getText().toString().trim().isEmpty()){
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                }
                else {
                    double result = Math.pow(Double.parseDouble(num1.getText().toString().trim()),
                            Double.parseDouble(num2.getText().toString().trim()));
                    textResult.setText(""+ result);
                }
            }
        });

        //Borrar
        btCe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textResult.setText("0.0");
            }
        });

    }

    public void aboutUs (View view){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, AboutUs.class);
                startActivity(intent);
                finish();
            }
        }, SPLASH_SCREEN);
    }
}