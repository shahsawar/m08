package com.example.menufloatingcontext;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ActivityListViewClass extends AppCompatActivity {

    private ListView listView;
    ArrayList<CustomItem> items = new ArrayList<>();

    public static String EXTRA_TEXT_TITLE = "com.example.menufloatingcontext.EXTRA_TEXT_TITLE";
    public static String EXTRA_TEXT_DESC = "com.example.menufloatingcontext.EXTRA_TEXT_DESC";
    public static String EXTRA_IMAGE = "com.example.menufloatingcontext.EXTRA_IMAGEESC";


    // Create object items
    CustomItem item1 = new CustomItem("Bike", R.drawable.bike, "Description of the item 1 in details activity, Description of the item 1 in details activity.");
    CustomItem item2 = new CustomItem("Boat", R.drawable.boat, "Description of the item 2 in details activity, Description of the item 2 in details activity.");
    CustomItem item3 = new CustomItem("Car", R.drawable.car, "Description of the item 3 in details activity, Description of the item 3 in details activity.");
    CustomItem item4 = new CustomItem("Construction", R.drawable.construction, "Description of the item 4 in details activity, Description of the item 4 in details activity.");
    CustomItem item5 = new CustomItem("Container", R.drawable.container, "Description of the item 5 in details activity, Description of the item 5 in details activity.");
    CustomItem item6 = new CustomItem("Order", R.drawable.order, "Description of the item 6 in details activity, Description of the item 6 in details activity.");
    CustomItem item7 = new CustomItem("Plane", R.drawable.plane, "Description of the item 7 in details activity, Description of the item 7 in details activity.");
    CustomItem item8 = new CustomItem("Train", R.drawable.train, "Description of the item 8 in details activity, Description of the item 8 in details activity.");
    CustomItem item9 = new CustomItem("Container", R.drawable.container, "Description of the item 9 in details activity, Description of the item 9 in details activity.");

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()){
            case 1:
                //Share
                Toast.makeText(this, "Details", Toast.LENGTH_SHORT).show();
                displayInfo(info.position);
                return true;
            case 2:
                //Delete
                Toast.makeText(this, "Share", Toast.LENGTH_SHORT).show();
                return true;
            case 3:
                //Web
                Toast.makeText(this, "Web", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);

        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Choose an options");
        menu.add(0, 1, 1, "Details");
        menu.add(0, 2, 2, "Share");
        menu.add(0, 3, 3, "Web");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_class);

        listView = findViewById(R.id.listView);

        // Add object items to array
        items.add(item1);
        items.add(item2);
        items.add(item3);
        items.add(item4);
        items.add(item5);
        items.add(item6);
        items.add(item7);
        items.add(item8);
        items.add(item9);

        CustomAdapter customAdapter = new CustomAdapter(this, items);
        listView.setAdapter(customAdapter);

        registerForContextMenu(listView);

    }

    private class CustomAdapter extends BaseAdapter {

        private Context context;
        private ArrayList<CustomItem> items;

        public CustomAdapter(Context context, ArrayList<CustomItem> items) {
            this.context = context;
            this.items = items;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.listview_row_data, null);
            TextView listTextRow = view.findViewById(R.id.listViewTextRow);
            ImageView listImageRow = view.findViewById(R.id.listViewImageRow);

            listTextRow.setText(items.get(position).getItemName());
            listImageRow.setImageResource(items.get(position).getImageName());

            return view;
        }
    }

    private void displayInfo(int position){
        Intent i = new Intent(getApplicationContext(), DetailActivity.class);
        i.putExtra(EXTRA_TEXT_TITLE, items.get(position).getItemName());
        i.putExtra(EXTRA_TEXT_DESC, items.get(position).getDescription());
        i.putExtra(EXTRA_IMAGE, items.get(position).getImageName());
        startActivity(i);
    }
}