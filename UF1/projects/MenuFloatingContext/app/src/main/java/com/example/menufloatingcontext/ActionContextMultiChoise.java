package com.example.menufloatingcontext;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ActionContextMultiChoise extends AppCompatActivity {

    private ListView listView;
    List<String> cursos = new ArrayList<>();
    List<String> itemSelectedList = new ArrayList<>();
    ArrayAdapter<String> arrayAdapter;
    private int selectedPosition;
    private int cont = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action_context_multi_choise);

        //Hook
        listView = findViewById(R.id.listViewMultiChoise);

        //Add courses in array
        cursos.add("1JSIM");
        cursos.add("2JSIM");
        cursos.add("1HISX");
        cursos.add("2HISX");
        cursos.add("1HISM");
        cursos.add("2HISM");
        cursos.add("1HIAW");
        cursos.add("2HIAW");
        cursos.add("1JISM");

        // Create and set arrayAdapter for listView
        arrayAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, cursos);
        listView.setAdapter(arrayAdapter);

        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                cont+=1;
                mode.setTitle(cont + " items selected");
                itemSelectedList.add(cursos.get(position));
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater menuInflater = mode.getMenuInflater();
                menuInflater.inflate(R.menu.my_menu, menu);
                mode.setTitle("Choose an option:");
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()){
                    case R.id.item_delete:
                        for (String s: itemSelectedList) {
                            arrayAdapter.remove(s);
                        }
                        Toast.makeText(ActionContextMultiChoise.this, cont + " items removed", Toast.LENGTH_SHORT).show();
                        cont=0;
                        mode.finish();
                        return true;
                        /*
                        arrayAdapter.remove(arrayAdapter.getItem(selectedPosition));
                        arrayAdapter.notifyDataSetChanged();
                        Toast.makeText(ActionContextMultiChoise.this, "Delete", Toast.LENGTH_SHORT).show();
                        return true;*/
                    case R.id.item_share:
                        Toast.makeText(ActionContextMultiChoise.this, "Share", Toast.LENGTH_SHORT).show();
                        return true;
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                mode = null;
            }
        });
    }
}