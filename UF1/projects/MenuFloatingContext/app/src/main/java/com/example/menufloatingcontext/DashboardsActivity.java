package com.example.menufloatingcontext;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class DashboardsActivity extends AppCompatActivity {

    private Button listViewText;
    private Button listViewImage;
    private Button gridViewText;
    private Button gridViewImage;
    private Button listViewClass;
    private Button actionContext;
    private Button actionContextMulti;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboards);

        //Hook
        listViewText = findViewById(R.id.listViewTextBt);
        listViewImage = findViewById(R.id.listViewImageBt);
        gridViewText = findViewById(R.id.gridViewTextBt);
        gridViewImage = findViewById(R.id.gridViewImageBt);
        listViewClass = findViewById(R.id.listViewClassBt);
        actionContext = findViewById(R.id.actionContext);
        actionContextMulti = findViewById(R.id.actionContextMultiBt);


        listViewText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirect(ActivityListViewText.class);
            }
        });

        listViewImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirect(ActivityListViewImage.class);
            }
        });

        gridViewText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirect(ActivityGridViewText.class);
            }
        });

        gridViewImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirect(ActivityGridViewImage.class);
            }
        });

        listViewClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirect(ActivityListViewClass.class);
            }
        });

        actionContext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirect(ActionContext.class);
            }
        });

        actionContextMulti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirect(ActionContextMultiChoise.class);
            }
        });

    }

    //Method which redirects us to an activity passed by parameter.
    public void redirect(Class activityToOpen){ //For listView
        Intent intent = new Intent(DashboardsActivity.this, activityToOpen);
        startActivity(intent);
    }

}