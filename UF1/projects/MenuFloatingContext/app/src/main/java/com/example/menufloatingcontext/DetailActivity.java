package com.example.menufloatingcontext;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    private TextView title;
    private TextView description;
    private ImageView image;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        title = findViewById(R.id.detailTitle);
        description = findViewById(R.id.detailDescription);
        image = findViewById(R.id.detailImage);


        Intent intent = getIntent();
        String text_Title = intent.getStringExtra(ActivityListViewClass.EXTRA_TEXT_TITLE);
        String text_Desc = intent.getStringExtra(ActivityListViewClass.EXTRA_TEXT_DESC);
        int image_View = intent.getIntExtra(ActivityListViewClass.EXTRA_IMAGE, 1);

        title.setText(text_Title);
        description.setText(text_Desc);
        image.setImageResource(image_View);
    }
}