package com.example.menufloatingcontext;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ActionContext extends AppCompatActivity {

    private ListView listView;
    List<String> cursos = new ArrayList<>();
    ArrayAdapter<String> arrayAdapter;
    private ActionMode actionMode;
    private int selectedPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action_context);

        //Hook
        listView = findViewById(R.id.listView);

        //Add curs in array
        cursos.add("1JSIM");
        cursos.add("2JSIM");
        cursos.add("1HISX");
        cursos.add("2HISX");
        cursos.add("1HISM");
        cursos.add("2HISM");
        cursos.add("1HIAW");
        cursos.add("2HIAW");
        cursos.add("1JISM");

        // Create and set arrayAdapter for listView
        arrayAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, cursos);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (actionMode != null){
                    return false;
                }
                selectedPosition = position;
                actionMode = startSupportActionMode(actionModeCallBack);
                return true;
            }
        });
    }

    ActionMode.Callback actionModeCallBack = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater menuInflater = mode.getMenuInflater();
            menuInflater.inflate(R.menu.my_menu, menu);
            mode.setTitle("Choose an option:");
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()){
                case R.id.item_delete:
                    arrayAdapter.remove(arrayAdapter.getItem(selectedPosition));
                    arrayAdapter.notifyDataSetChanged();
                    Toast.makeText(ActionContext.this, "Delete", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.item_share:
                    Toast.makeText(ActionContext.this, "Share", Toast.LENGTH_SHORT).show();
                    return true;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            actionMode = null;
        }
    };
}