package com.example.menufloatingcontext;

public class CustomItem {

    private String itemName;
    private int imageName;
    private  String description;

    public CustomItem(String itemName, int imageName, String description) {
        this.itemName = itemName;
        this.imageName = imageName;
        this.description = description;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getImageName() {
        return imageName;
    }

    public void setImageName(int imageName) {
        this.imageName = imageName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
