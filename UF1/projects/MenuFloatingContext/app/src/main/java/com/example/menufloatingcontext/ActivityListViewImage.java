package com.example.menufloatingcontext;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityListViewImage extends AppCompatActivity {

    private ListView listView;
    private Integer[] imageIDs = {
            R.drawable.bike,
            R.drawable.boat,
            R.drawable.car,
            R.drawable.construction,
            R.drawable.container,
            R.drawable.order,
            R.drawable.plane,
            R.drawable.train,
            R.drawable.container
    };
    private String[] imageText = {"Bike", "Boat", "Car", "Construction", "Container", "Order", "Plane", "Train", "Container"};


    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case 1:
                //Share
                Toast.makeText(this, "Share", Toast.LENGTH_SHORT).show();
                return true;
            case 2:
                //Delete
                Toast.makeText(this, "Delete", Toast.LENGTH_SHORT).show();
                return true;
            case 3:
                //Web
                Toast.makeText(this, "Web", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);

        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Choose an options");
        menu.add(0, 1, 1, "Share");
        menu.add(0, 2, 2, "Delete");
        menu.add(0, 3, 3, "Web");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_image);

        listView = findViewById(R.id.listView);

        CustomAdapter customAdapter = new CustomAdapter();
        listView.setAdapter(customAdapter);

        // Necessari cridar  registerForContextMenu, per en registrar els items del grid al nostre menú de context.
        registerForContextMenu(listView);

    }


    private class CustomAdapter extends BaseAdapter {



        @Override
        public int getCount() {
            return imageIDs.length;
        }

        @Override
        public Object getItem(int position) {
            return imageIDs[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.listview_row_data, null);
            TextView listTextRow = view.findViewById(R.id.listViewTextRow);
            ImageView listImageRow = view.findViewById(R.id.listViewImageRow);

            listTextRow.setText(imageText[position]);
            listImageRow.setImageResource(imageIDs[position]);

            return view;
        }
    }
}