package com.example.menufloatingcontext;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ActivityGridViewImage extends AppCompatActivity {

    private GridView gridView;
    private Integer[] imageIDs = {
            R.drawable.bike,
            R.drawable.boat,
            R.drawable.car,
            R.drawable.construction,
            R.drawable.container,
            R.drawable.order,
            R.drawable.plane,
            R.drawable.train,
            R.drawable.container
    };
    private String[] imageText = {"Bike", "Boat", "Car", "Construction", "Container", "Order", "Plane", "Train", "Container"};


    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case 1:
                //Share
                Toast.makeText(this, "Share", Toast.LENGTH_SHORT).show();
                return true;
            case 2:
                //Delete
                Toast.makeText(this, "Delete", Toast.LENGTH_SHORT).show();
                return true;
            case 3:
                //Web
                Toast.makeText(this, "Web", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);

        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Choose an options");
        menu.add(0, 1, 1, "Share");
        menu.add(0, 2, 2, "Delete");
        menu.add(0, 3, 3, "Web");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view_image);

        gridView = findViewById(R.id.gridView);

        CustomAdapter customAdapter = new CustomAdapter();
        gridView.setAdapter(customAdapter);

        // Necessari cridar  registerForContextMenu, per en registrar els items del grid al nostre menú de context.
        registerForContextMenu(gridView);
    }


    private class CustomAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return imageIDs.length;
        }

        @Override
        public Object getItem(int position) {
            return imageIDs[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.gridview_row_data, null);
            TextView gridTextRow = view.findViewById(R.id.gridViewTextView);
            ImageView gridImageRow = view.findViewById(R.id.gridViewImageView);

            gridTextRow.setText(imageText[position]);
            gridImageRow.setImageResource(imageIDs[position]);

            return view;
        }
    }
}