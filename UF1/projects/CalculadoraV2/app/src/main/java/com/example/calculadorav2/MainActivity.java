package com.example.calculadorav2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final String HELLO_FROM_MAIN = "com.example.calculadav2.HELLO_FROM_MAIN";

    private TextView textResult;
    private EditText num1;
    private EditText num2;

    private Button btSum;
    private Button btRes;
    private Button btDiv;
    private Button btMult;
    private Button btPow;
    private Button btCE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hooks
        textResult = findViewById(R.id.textView);
        num1 = findViewById(R.id.num1);
        num2 = findViewById(R.id.num2);
        btSum = findViewById(R.id.sumBt);
        btRes = findViewById(R.id.resBt);
        btDiv = findViewById(R.id.divBt);
        btMult = findViewById(R.id.multBt);
        btPow = findViewById(R.id.powBt);
        btCE = findViewById(R.id.ceBt);

        btSum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFieldNull()){
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                }else {
                    textResult.setText(sum()+"");
                }
            }
        });

        btRes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFieldNull()){
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                }else {
                    textResult.setText(res()+"");
                }
            }
        });

        btDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFieldNull()){
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                }else {
                    if (isDivisible()){
                        textResult.setText(div()+"");
                    }else {
                        Toast.makeText(MainActivity.this, "Divide by zero impossible", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btMult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFieldNull()){
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                }else {
                    textResult.setText(mult()+"");
                }
            }
        });

        btPow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFieldNull()){
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                }else {
                    textResult.setText(pow()+"");
                }
            }
        });

        btCE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textResult.setText(0+"");
                num1.setText(null);
                num2.setText(null);

            }
        });
    }

    public double sum(){
        double n1 = Double.parseDouble(num1.getText().toString());
        double n2 = Double.parseDouble(num2.getText().toString());
        double result = n1 + n2;
        return  result;
    }

    public double res(){
        double n1 = Double.parseDouble(num1.getText().toString());
        double n2 = Double.parseDouble(num2.getText().toString());
        double result = n1 - n2;
        return  result;
    }

    public double mult(){
        double n1 = Double.parseDouble(num1.getText().toString());
        double n2 = Double.parseDouble(num2.getText().toString());
        double result = n1 * n2;
        return  result;
    }

    public double div(){
        double n1 = Double.parseDouble(num1.getText().toString());
        double n2 = Double.parseDouble(num2.getText().toString());
        double result = n1 / n2;
        return  result;
    }

    public boolean isDivisible(){
        if (num2.getText().toString().equals("0")){
            return false;
        }
        return true;
    }

    public double pow(){
        double n1 = Double.parseDouble(num1.getText().toString());
        double n2 = Double.parseDouble(num2.getText().toString());
        double result = Math.pow(n1, n2);
        return  result;
    }

    public boolean isFieldNull(){
        if (num1.getText().toString().trim().isEmpty() || num2.getText().toString().trim().isEmpty()){
            return true;
        }
        return false;
    }
    public void aboutUs(View view) {
        Intent intent = new Intent(MainActivity.this, aboutUs.class);
        intent.putExtra(HELLO_FROM_MAIN, "Hello From Main");
        startActivity(intent);
        //finish();
    }
}