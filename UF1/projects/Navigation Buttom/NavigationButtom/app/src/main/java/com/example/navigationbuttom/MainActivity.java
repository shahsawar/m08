package com.example.navigationbuttom;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView mMainNav;
    private FrameLayout mMainFrame;
    private Fragment homeFragment;
    private Fragment profileFragment;
    private Fragment settingsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMainNav = findViewById(R.id.main_nav);
        mMainFrame = findViewById(R.id.main_frame);
        mMainNav.setItemBackgroundResource(R.color.colorPrimaryDark);

        //homeFragment = new homeFragment();
        //profileFragment = new ProfileFragment();
        settingsFragment = new SettingsFragment();

        homeFragment = HomeFragment.newInstance("Home1", "Home2");
        profileFragment = ProfileFragment.newInstance("Profile1", "Profile2");
        //settingsFragment = SettingsFragment.newInstance("Settings1", "Settings2");


        displayFragment(homeFragment);

        mMainNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nav_home:
                        mMainNav.setItemBackgroundResource(R.color.colorPrimaryDark);
                        displayFragment(homeFragment);
                        return true;
                    case R.id.nav_profile:
                        mMainNav.setItemBackgroundResource(R.color.colorPrimary);
                        displayFragment(profileFragment);
                        return true;
                    case R.id.nav_settings:
                        mMainNav.setItemBackgroundResource(R.color.colorAccent);
                        displayFragment(settingsFragment);
                        return true;
                }
                return false;
            }
        });
    }

    private void displayFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment);
        fragmentTransaction.commit();
    }
}