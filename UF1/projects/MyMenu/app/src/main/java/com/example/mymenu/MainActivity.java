package com.example.mymenu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.my_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.itemShop:
                Toast.makeText(this, "Shop", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.itemRent:
                Toast.makeText(this, "Rent", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.itemProfile:
                Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.itemSettings:
                Toast.makeText(this, "Settings", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.itemMountain:
                Toast.makeText(this, "Mountain Bikes", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.itemCity:
                Toast.makeText(this, "City Bikes", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.itemElectric:
                Toast.makeText(this, "Electric Bikes", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}