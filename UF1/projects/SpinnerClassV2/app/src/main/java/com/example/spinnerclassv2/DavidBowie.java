package com.example.spinnerclassv2;

import android.content.Context;

import java.util.ArrayList;

public class DavidBowie {
    private String coverName;
    private int imgCover;

    public DavidBowie(String coverName, int imgCover) {
        this.coverName = coverName;
        this.imgCover = imgCover;
    }

    public DavidBowie(Context context, int i, ArrayList<DavidBowie> davidBowieArrayList) {

    }

    public String getCoverName() {
        return coverName;
    }

    public void setCoverName(String coverName) {
        this.coverName = coverName;
    }

    public int getImgCover() {
        return imgCover;
    }

    public void setImgCover(int imgCover) {
        this.imgCover = imgCover;
    }
}
