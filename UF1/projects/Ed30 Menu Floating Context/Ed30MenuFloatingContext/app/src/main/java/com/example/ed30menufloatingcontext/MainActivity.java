package com.example.ed30menufloatingcontext;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView listView;

    List<String> cursos = new ArrayList<>();
    ArrayAdapter<String> dataAdapter;

    ListView listView1;

    private Integer[] imageIDs = {
            R.drawable.flight,
            R.drawable.flight,
            R.drawable.flight,
            R.drawable.flight,
            R.drawable.flight,
            R.drawable.flight,
            R.drawable.flight,
    };

    String[] imageText = {"flight", "Construction", "Containers", "Planes", "Trains", "Transports"};

    CustomAdapter customAdapter = new CustomAdapter();


    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case 1:
                //Share
                Toast.makeText(this, "Share", Toast.LENGTH_SHORT).show();
                return true;
            case 2:
                //Delete
                Toast.makeText(this, "Delete", Toast.LENGTH_SHORT).show();
                return true;
            case 3:
                //Web
                Toast.makeText(this, "Web", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);

        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Choose an options");
        menu.add(0, 1, 1, "Share");
        menu.add(0, 2, 2, "Delete");
        menu.add(0, 3, 3, "Web");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hook
        listView = findViewById(R.id.listView);

        //Add curs in array
        cursos.add("1JSIM");
        cursos.add("2JSIM");
        cursos.add("1HISX");
        cursos.add("2HISX");
        cursos.add("1HISM");
        cursos.add("2HISM");
        cursos.add("1HIAW");
        cursos.add("2HIAW");
        cursos.add("1JISM");

        //DataAdapter
        dataAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, cursos);
        listView.setAdapter(dataAdapter);

        // Necessari cridar  registerForContextMenu, per en registrar els items del grid al nostre menú de context.
        registerForContextMenu(listView);


    }

    private class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return imageIDs.length;
        }

        @Override
        public Object getItem(int position) {
            return imageIDs[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.list_layout, null);
            //Hook
            TextView textView = view.findViewById(R.id.textView);
            ImageView imageView = view.findViewById(R.id.imageView);

            //SetText i setImage
            textView.setText(imageText[position]);
            imageView.setImageResource(imageIDs[position]);

            return view;
        }
    }
}