package com.example.ed30menufloatingcontext;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity2 extends AppCompatActivity {

    private ListView listView;
    Item item = new Item("Flight", "Go on air", R.drawable.flight, "From: 100€", "Description flight", R.drawable.flight);
    Item item2 = new Item("Flight2", "Go on air2", R.drawable.flight, "From: 200€", "Description flight", R.drawable.flight);
    Item item3 = new Item("Flight3", "Go on air3", R.drawable.flight, "From: 300€", "Description flight", R.drawable.flight);

    CustomAdapter customAdapter = new CustomAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        listView = findViewById(R.id.listView);

        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        items.add(item2);
        items.add(item3);


    }

    private class CustomAdapter extends BaseAdapter {

        private Context context;
        private ArrayList<Item> item;

        @Override
        public int getCount() {
            return item.size();
        }

        @Override
        public Object getItem(int position) {
            return item.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            //Inflar el menu row
            View view = getLayoutInflater().inflate(R.layout.list_layout, null);

            //Declara les var i fer el hook
            TextView textRow = view.findViewById(R.id.textView);
            ImageView imageRow = findViewById(R.id.imageView);

            //setText i setImageResource
            textRow.setText(item.get(position).getItemName());
            imageRow.setImageResource(item.get(position).getImageName());


            return null;
        }
    }
}