package com.example.ed30menufloatingcontext;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;

public class DashboardActivity extends AppCompatActivity {

    private Button listViewBt;
    private Button gridViewTextBt;
    private Button gridViewImageBt;
    private Button listViewFloatingBt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
    }
}