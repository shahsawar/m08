package com.example.exercicilayouts;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.media.Image;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private MediaPlayer mediaPlayer;

    ObjectAnimator objectAnimator1;
    ObjectAnimator objectAnimator2;
    ObjectAnimator objectAnimator3;
    ObjectAnimator objectAnimator4;
    ObjectAnimator objectAnimator5;

    private ImageView imageView1;
    private ImageView imageView1_2;
    private ImageView imageView2;
    private ImageView imageView3;
    private ImageView imageView4;
    private ImageView imageView5;

    boolean turn = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hook
        imageView1 = (ImageView) findViewById(R.id.imageView1);
        imageView1_2 = (ImageView) findViewById(R.id.imageView1_2);
        imageView2 = (ImageView) findViewById(R.id.imageView2);
        imageView3 = (ImageView) findViewById(R.id.imageView3);
        imageView4 = (ImageView) findViewById(R.id.imageView4);
        imageView5 = (ImageView) findViewById(R.id.imageView5);



        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(turn){
                    objectAnimator1 = ObjectAnimator.ofFloat(imageView1, "rotationY", 0f, 180f);
                    objectAnimator2 = ObjectAnimator.ofFloat(imageView1_2, "rotationY", 180f, 0f);
                    objectAnimator2 = ObjectAnimator.ofFloat(imageView1_2, "alpha", 1f, 0.2f);
                    objectAnimator1 = ObjectAnimator.ofFloat(imageView1, "alpha", 0f, 0.2f);
                    //objectAnimator2 = ObjectAnimator.ofFloat(imageView1_2, "alpha", 1f, 0.2f);
                    turn = false;
                }else{
                    //objectAnimator2 = ObjectAnimator.ofFloat(imageView1_2, "rotationY", 180f, 0f);
                    //objectAnimator1 = ObjectAnimator.ofFloat(v, "alpha", 0f,0.2f);

                    turn = true;
                }
                objectAnimator1.setDuration(1000);
                objectAnimator2.setDuration(1);
                objectAnimator2.setStartDelay(500);
                objectAnimator1.start();
                objectAnimator2.start();

                /*
                AnimatorSet set;
                if (turn){
                    set = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.frontanimator);
                    turn = false;
                }else{
                    set = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.backanimator);
                    turn = true;
                }

                set.setTarget(v);
                set.start();*/
            }
        });
    }
}