package com.example.spinnertest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class DavidBowiedAdapter extends ArrayAdapter<DavidBowie> {

    public DavidBowiedAdapter(Context context, ArrayList<DavidBowie> davidBowieArrayList) {
        super(context, 0, davidBowieArrayList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return init(position, convertView, parent);
    }


    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return init(position, convertView, parent);
    }


    public View init(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        if (convertView == null){
            //Inflarem el convertView
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.activity_main,
                    parent,
                    false
            );
        }
        //Hook
        ImageView imageView = convertView.findViewById(R.id.imageView);
        TextView textView = convertView.findViewById(R.id.textView);

        DavidBowie currentDavidBowie = getItem(position);
        if (currentDavidBowie != null){
            textView.setText(currentDavidBowie.getCoverName());
            imageView.setImageResource(currentDavidBowie.getImgCover());
        }
        return convertView;
    }

}
