package com.example.spinnertest;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Spinner spinnerClass;
    private ArrayList<DavidBowie> mDavidBowie;
    private DavidBowiedAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hook
        spinnerClass = findViewById(R.id.spinnerClass);

        //Instanciar DBs
        mDavidBowie = new ArrayList<>();
        mDavidBowie.add(new DavidBowie("Stardust", R.drawable.db8));
        mDavidBowie.add(new DavidBowie("Aladin Sane", R.drawable.db6));
        mDavidBowie.add(new DavidBowie("low", R.drawable.db7));
        mDavidBowie.add(new DavidBowie("Heroes", R.drawable.db3));

        //Spinner class
        mAdapter = new DavidBowiedAdapter(this, mDavidBowie);
        spinnerClass.setAdapter(mAdapter);

        spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                DavidBowie currentDavidBowie = (DavidBowie) parent.getItemAtPosition(position);
                String coverName = currentDavidBowie.getCoverName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
}