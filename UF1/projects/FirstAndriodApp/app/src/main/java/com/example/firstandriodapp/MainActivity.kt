package com.example.firstandriodapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_main.*

public class MainActivity extends AppCompatActivity() {
//class MainActivity : AppCompatActivity() {

    private EditText num1;
    private EditText num2;
    private TextView textResult;
    private Button add;
    private Button subs;
    private Button mult;
    private Button div;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Hook = enllaçar

        num1 = (EditText) findViewById(R.id.num1);
        num1 = (EditText) findViewById(R.id.num2);
        textResult = (textView) findViewById(R.id.textResult);
        add = (Button) findById(R.id.btAdd);
        subs = (Button) findById(R.id.btSubs);
        mult = (Button) findById(R.id.btMult);
        div = (Button) findById(R.id.btDiv);


        add.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Double result =  Double.parseDouble(num1.getText().toString().trim()) + Double.parseDouble(num2.getText().toString().trim());
                textResult.setText(result + "");
            }
        });

        subs.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Double result =  Double.parseDouble(num1.getText().toString().trim()) - Double.parseDouble(num2.getText().toString().trim());
                textResult.setText(result + "");
            }
        });

        mult.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Double result =  Double.parseDouble(num1.getText().toString().trim()) * Double.parseDouble(num2.getText().toString().trim());
                textResult.setText(result + "");
            }
        });

        div.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Double result =  Double.parseDouble(num1.getText().toString().trim()) / Double.parseDouble(num2.getText().toString().trim());
                textResult.setText(result + "");
            }
        });

    }
}