package com.example.firebaseactivity;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class MyImageAdapter extends RecyclerView.Adapter<MyImageAdapter.MyImageViewHolder> {

    private Context mContext;
    private List<Uri> listUrls;

    public MyImageAdapter(Context mContext, List<Uri> listUrls) {
        this.mContext = mContext;
        this.listUrls = listUrls;
    }

    @NonNull
    @Override
    public MyImageAdapter.MyImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.my_image_row, parent, false);
        return new MyImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyImageAdapter.MyImageViewHolder holder, int position) {
        //Picasso.get().load(listUrls.get(position)).into(holder.imageView);
        Picasso.get().load(listUrls.get(position)).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return listUrls.size();
    }

    public class MyImageViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;

        public MyImageViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.myImageRow);
        }
    }
}
