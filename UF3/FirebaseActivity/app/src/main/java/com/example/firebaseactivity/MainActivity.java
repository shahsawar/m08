package com.example.firebaseactivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mFirebaseAuth;
    private TextInputEditText username;
    private TextInputEditText password;
    private TextInputEditText inputPhone;
    private TextInputEditText inputVerificationCode;
    private Button signup;
    private Button login;
    private Button google;
    private Button sendPhoneNumberBtn;
    private Button verificationPhoneNumberBtn;

    private GoogleSignInOptions gso;
    private GoogleSignInClient signInClient;
    private static final int GOOGLE_SING_IN_CODE = 10005;

    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private static final String TAG = "PhoneAuthActivity";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GOOGLE_SING_IN_CODE) {
            Task<GoogleSignInAccount> signInTask = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount signInAccount = signInTask.getResult(ApiException.class);
                AuthCredential authCredential = GoogleAuthProvider.getCredential(signInAccount.getIdToken(), null);
                mFirebaseAuth.signInWithCredential(authCredential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Toast.makeText(getApplicationContext(), "Your google account is connected to our aplication", Toast.LENGTH_SHORT).show();
                        dataGmail();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        showAlert();
                    }
                });
            } catch (ApiException e) {
                e.printStackTrace();
                showAlert();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hooks
        mFirebaseAuth = FirebaseAuth.getInstance();

        username = findViewById(R.id.inputUsername);
        password = findViewById(R.id.inputUserPassword);
        inputPhone = findViewById(R.id.inputUserPhone);
        inputVerificationCode = findViewById(R.id.inputUserVerificationCode);

        signup = findViewById(R.id.deleteBtn);
        login = findViewById(R.id.saveBtn);
        google = findViewById(R.id.google);
        sendPhoneNumberBtn = findViewById(R.id.phoneNumber);
        verificationPhoneNumberBtn = findViewById(R.id.verifyCodeBtn);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((!username.getText().toString().isEmpty()) && (!password.getText().toString().isEmpty())) {
                    createUser();
                }
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((!username.getText().toString().isEmpty()) && (!password.getText().toString().isEmpty())) {
                    loginUser();
                }
            }
        });


        //SignIn with google
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))  //firebase id
                .requestEmail()   //demanem mail usuari
                .build();

        signInClient = GoogleSignIn.getClient(this, gso);

        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Configure Google Sign In
                signInClient.signOut();
                Intent sign = signInClient.getSignInIntent();
                startActivityForResult(sign, GOOGLE_SING_IN_CODE);
            }
        });


        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                Log.d(TAG, "onVerificationCompleted:" + phoneAuthCredential);
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e);

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                }

                // Show a message and update the UI
            }

            @Override
            public void onCodeSent(@NonNull String verificationId,
                                   @NonNull PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;
            }
        };

        sendPhoneNumberBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhoneAuthOptions options =
                        PhoneAuthOptions.newBuilder(mFirebaseAuth)
                                .setPhoneNumber(inputPhone.getText().toString())  // Phone number to verify
                                .setTimeout(60L, TimeUnit.SECONDS)   // Timeout and unit
                                .setActivity(MainActivity.this)             // Activity (for callback binding)
                                .setCallbacks(mCallbacks)                   // OnVerificationStateChangedCallbacks
                                .build();
                PhoneAuthProvider.verifyPhoneNumber(options);
            }
        });

        verificationPhoneNumberBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, inputVerificationCode.getText().toString());
                signInWithPhoneAuthCredential(credential);
            }
        });
    }

    //Create a user
    public void createUser() {
        mFirebaseAuth.createUserWithEmailAndPassword(
                username.getText().toString(),
                password.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            data();
                        } else {
                            Toast.makeText(MainActivity.this, "Email/Password is incorrect or user already exists!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    //Login user
    public void loginUser() {
        mFirebaseAuth.signInWithEmailAndPassword(username.getText().toString(), password.getText().toString()).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    data();
                } else {
                    showAlert();
                }
            }
        });
    }

    //Pass data to DataActivity
    public void data() {
        Intent intent = new Intent(this, ActivityProfile.class);
        intent.putExtra("username", username.getText().toString());
        intent.putExtra("password", password.getText().toString());
        startActivity(intent);
    }

    public void dataPhone() {
        startActivity(new Intent(this, ActivityProfile.class));
    }

    private void dataGmail() {
        Intent intent = new Intent(this, ActivityProfile.class);
        intent.putExtra("username", mFirebaseAuth.getCurrentUser().getEmail());
        intent.putExtra("password", "");
        intent.putExtra("photoUrl", mFirebaseAuth.getCurrentUser().getPhotoUrl());
        intent.putExtra("displayName", mFirebaseAuth.getCurrentUser().getDisplayName());
        intent.putExtra("providerId", mFirebaseAuth.getCurrentUser().getProviderId());
        intent.putExtra("phoneNumber", mFirebaseAuth.getCurrentUser().getPhoneNumber());
        startActivity(intent);
        finish();
    }

    public void showAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("User or password does not exist.");
        builder.setTitle("Error");
        builder.setPositiveButton("Accept", null);
        builder.create();
        builder.show();
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mFirebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");

                            FirebaseUser user = task.getResult().getUser();
                            // Update UI
                            dataPhone();
                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                            }
                        }
                    }
                });
    }
}