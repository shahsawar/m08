package com.example.firebaseactivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class ActivityList extends AppCompatActivity {

    private RecyclerView recyclerView;
    private TextInputEditText userEmail;
    private TextInputEditText name;
    private TextInputEditText dni;
    private Button save;
    private Button load;
    private Button images;


    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference list = db.collection("List");

    List<User> userList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        //Hook
        userEmail = findViewById(R.id.activityListInputEmail);
        name = findViewById(R.id.activityListInputNAME);
        dni = findViewById(R.id.activityListInputDNI);
        save = findViewById(R.id.activityListSaveBtn);
        load = findViewById(R.id.activityListLoadBtn);
        images = findViewById(R.id.activityListImageBtn);
        recyclerView = findViewById(R.id.myRecyclerView);

        userEmail.setText(getIntent().getStringExtra("useremail"));


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                User userTmp = new User();
                userTmp.setName(name.getText().toString());
                userTmp.setDni(dni.getText().toString());
                userTmp.setEmail(userEmail.getText().toString());

                /*
                list.document(FirebaseAuth.getInstance().getUid()).set(userTmp).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(ActivityList.this, "Data added.", Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(ActivityList.this, "Error adding data.", Toast.LENGTH_SHORT).show();
                    }
                });*/


                list.add(userTmp).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(ActivityList.this, "Data added.", Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(ActivityList.this, "Error adding data.", Toast.LENGTH_SHORT).show();
                        Log.d("onFailure", e.toString());
                    }
                });
            }
        });


        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = FirebaseAuth.getInstance().getCurrentUser().getEmail();

                list.whereGreaterThanOrEqualTo("email", email).orderBy("email", Query.Direction.DESCENDING).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                                User userTmp = documentSnapshot.toObject(User.class);
                                userList.add(userTmp);
                        }
                        MyAdapter myadapter = new MyAdapter(userList, getApplicationContext());
                        recyclerView.setAdapter(myadapter);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    }
                });
            }
        });

        images.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityList.this, ActivityImage.class));
            }
        });

    }
}