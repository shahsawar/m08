package com.example.firebaseactivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

public class ActivityProfile extends AppCompatActivity {

    private FirebaseFirestore db;
    private TextView email;
    private TextView password;
    private TextView dni;
    private TextView name;
    private TextView lastname;
    private TextView age;
    private Button saveButton;
    private Button deleteButton;
    private Button logoutButton;
    private Button dataListButton;


    @Override
    protected void onStart() {
        super.onStart();
        db = FirebaseFirestore.getInstance();
        Toast.makeText(this, ""+FirebaseAuth.getInstance().getUid(), Toast.LENGTH_LONG).show();
        db.collection("users").document(FirebaseAuth.getInstance().getUid())
                .addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                        if (error != null) {
                            Toast.makeText(ActivityProfile.this, "Error while loading!", Toast.LENGTH_SHORT).show();
                            Log.d("TAG", error.toString());
                        }

                        if (value.exists()) {
                            User user = value.toObject(User.class);
                            email.setText(user.getEmail());
                            dni.setText(user.getDni());
                            name.setText(user.getName());
                            lastname.setText(user.getLastname());
                            age.setText(String.valueOf(user.getAge()));

                            Toast.makeText(ActivityProfile.this, "Data added.", Toast.LENGTH_SHORT).show();
                        } else {
                            if (email == null){
                                email.setText("");
                            }
                            dni.setText("");
                            name.setText("");
                            lastname.setText("");
                            age.setText("");
                        }
                    }
                });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        //hook
        db = FirebaseFirestore.getInstance();
        email = findViewById(R.id.ActivityInputUsername);
        password = findViewById(R.id.userpasswordInput);
        dni = findViewById(R.id.ActivityInputDni);
        name = findViewById(R.id.ActivityInputName);
        lastname = findViewById(R.id.ActivityInputLastname);
        age = findViewById(R.id.ActivityInputAge);
        saveButton = findViewById(R.id.saveBtn);
        deleteButton = findViewById(R.id.deleteBtn);
        logoutButton = findViewById(R.id.logoutBtn);
        dataListButton = findViewById(R.id.dataListBtn);

        getData();


        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Create user with data
                User userTmp = new User();
                userTmp.setEmail(email.getText().toString());
                userTmp.setDni(dni.getText().toString());
                userTmp.setName(name.getText().toString());
                userTmp.setLastname(lastname.getText().toString());

                if (!age.getText().toString().isEmpty()){
                    userTmp.setAge(Integer.parseInt(age.getText().toString()));
                }

                db.collection("users").document(FirebaseAuth.getInstance().getUid()).set(userTmp)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(ActivityProfile.this, "The user has successfully added", Toast.LENGTH_SHORT).show();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(ActivityProfile.this, "Error adding user!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.collection("users").document(FirebaseAuth.getInstance().getUid()).delete()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(ActivityProfile.this, "User successfully deleted.", Toast.LENGTH_SHORT).show();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(ActivityProfile.this, "Error while deleting user!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                onBackPressed();
            }
        });

        dataListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityProfile.this, ActivityList.class);
                intent.putExtra("useremail",FirebaseAuth.getInstance().getCurrentUser().getEmail());
                startActivity(intent);
            }
        });
    }


    public void getData() {
        Intent intent = getIntent();
        email.setText(intent.getStringExtra("username"));
        password.setText(intent.getStringExtra("password"));
    }
}