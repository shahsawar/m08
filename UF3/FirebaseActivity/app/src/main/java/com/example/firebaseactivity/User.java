package com.example.firebaseactivity;

public class User {
    private String email;
    private String dni;
    private String name;
    private String lastname;
    private int age;

    public User() {
    }

    public User(String email, String dni, String name, String lastname, int age) {
        this.email = email;
        this.dni = dni;
        this.name = name;
        this.lastname = lastname;
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getDni() {
        return dni;
    }

    public User setDni(String dni) {
        this.dni = dni;
        return this;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getLastname() {
        return lastname;
    }

    public User setLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public int getAge() {
        return age;
    }

    public User setAge(int age) {
        this.age = age;
        return this;
    }
}
